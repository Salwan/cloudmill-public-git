package  
{
	import flash.display.IBitmapDrawable;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	import cmg.Reference;
	
	/**
	 * Manages all intelligent creatures, and humans.
	 * @author ZenithSal
	 */
	public class CharacterManager 
	{
		protected static var charList:Vector.<Character> = null;
		protected static var charRoomDict:Dictionary = null;
		protected static var traceList:Array = null;
		protected static var traceIndex:int = 0;
		protected static var isInit:Boolean = false;
		
		public function CharacterManager() 
		{
			init();
		}
		
		protected static function init():void
		{
			charList = new Vector.<Character>();
			charRoomDict = new Dictionary(true);
			traceList = new Array();
			traceIndex = 0;
			isInit = true;
		}
		
		public static function addCharacter(char:Character):void
		{
			if (!isInit)
			{
				init();
			}
			charList.push(char);
		}
		
		public static function removeCharacter(char:Character):void
		{
			charList.splice(charList.indexOf(char), 1);
		}
		
		public static function announceNewPosition(char:Character):void
		{
			var room:Room = RoomManager.getRoomAt(char.x, char.y);
			var c:Object = null;
			if (room)
			{
				if (charRoomDict[char] != room)
				{
					// Notify other characters in the room that this one just entered
					for (c in charRoomDict)
					{
						if (charRoomDict[c] == room)
						{
							Character(c).onCharEnterRoom(char);
						}
					}
					char.onRoomEnter(room);
					charRoomDict[char] = room;					
				}
			}
			else
			{
				var prev:Room = charRoomDict[char];
				if (prev)
				{
					// Notify other characters in the room the this one is leaving
					for (c in charRoomDict)
					{
						if (charRoomDict[c] == room)
						{
							Character(c).onCharEnterRoom(char);
						}
					}
					char.onRoomExit(prev);
					charRoomDict[char] = null;
				}
			}
			
			updateTracedPosition(char, char.x, char.y);
		}
		
		public static function whereAmI(char:Character):Room
		{
			return RoomManager.getRoomAt(char.x, char.y);
		}
		
		public static function whoseHere(room:Room):Array
		{
			var a:Array = new Array();
			for(var c:Object in charRoomDict)
			{
				if (charRoomDict[c] == room)
				{
					a.push(Character(c));
				}
			}
			return a;
		}
		
		/**
		 * Start tracing player movement.
		 * @param	char
		 * @return identifier of trace data
		 */
		public static function traceCharacterMovement(char:Character):int 
		{
			traceIndex += 1;
			traceList[traceIndex] = new Object();
			traceList[traceIndex]["char"] = char;
			traceList[traceIndex]["trace"] = new Array(); // an array of points
			traceList[traceIndex]["lastindex"] = -1; // last traced point index (arrays are sparse)
			
			trace("started tracing ", char.id, " using id:", traceIndex);
			
			// At least the current position should be in the list
			updateTracedPosition(char, char.x, char.y);
			
			return traceIndex;
		}
		
		/**
		 * Will stop tracing movement, and clear all previous data
		 * @param	trace_id
		 */
		public static function stopTracingMovement(trace_id:int):void
		{
			if (traceList[trace_id])
			{
				traceList.splice(trace_id, 1);
				trace("stopped tracing: ", trace_id);
			}
		}
		
		/**
		 * 
		 * @param	trace_id
		 * @param	waypoint_index
		 * @return next waypoint index, or -1 if this is the last waypoint
		 */
		public static function getMovementWaypoint(trace_id:int, waypoint_index:int, pointRef:Reference):int
		{
			if (traceList[trace_id])
			{
				if (waypoint_index < traceList[trace_id]["trace"].length)
				{
					pointRef.value = traceList[trace_id]["trace"][waypoint_index];
					return waypoint_index + 1;
				}
				else
				{
					return -1;
				}
			}
			else
			{
				trace("ERROR: CharacterManager.getMovementWaypoint: trace_id doesn't exist");
				return -1;
			}
		}
		
		public static function updateTracedPosition(char:Character, _x:int, _y:int):void
		{
			_x = Math.floor(_x / 32);
			_y = Math.floor(_y / 32);
			for each(var value:Object in traceList)
			{
				if (value && value["char"] == char)
				{
					if (value["lastindex"] == -1) // This is first update
					{
						value["lastindex"] += 1;
						value["trace"][value["lastindex"]] = new Point(_x, _y);
						trace("first position trace: ", _x, _y);
					}
					else if (value["trace"][value["lastindex"]].x != _x || 
						value["trace"][value["lastindex"]].y != _y) // a new point
					{
						value["lastindex"] += 1;
						value["trace"][value["lastindex"]] = new Point(_x, _y);
						trace("position trace: ", _x, _y);
					}
					else
					{
						// this is the same last position, so no updates
					}
				}
			}
		}
		
		public static function onDead(char:Character):void
		{
			removeCharacter(char);
			delete charRoomDict[char];
		}
	}

}