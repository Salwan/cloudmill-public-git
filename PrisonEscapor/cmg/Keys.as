package cmg
{
	public class Keys 
	{
		
		public function Keys() 
		{			
		}
		
		public static const KC_UP = 38;
		public static const KC_RIGHT = 39;
		public static const KC_DOWN = 40;
		public static const KC_LEFT = 37;
		public static const KC_SPACE = 32;
		public static const KC_ENTER = 13;
		public static const KC_TAB = 9;
		public static const KC_ESCAPE = 27;
		
		public static const KC_A = 65;
		public static const KC_B = 66;
		public static const KC_C = 67;
		public static const KC_D = 68;
		public static const KC_E = 69;
		public static const KC_F = 70;
		public static const KC_G = 71;
		public static const KC_H = 72;
		public static const KC_I = 73;
		public static const KC_J = 74;
		public static const KC_K = 75;
		public static const KC_L = 76;
		public static const KC_M = 77;
		public static const KC_N = 78;
		public static const KC_O = 79;
		public static const KC_P = 80;
		public static const KC_Q = 81;
		public static const KC_R = 82;
		public static const KC_S = 83;
		public static const KC_T = 84;
		public static const KC_U = 85;
		public static const KC_V = 86;
		public static const KC_W = 87;
		public static const KC_X = 88;
		public static const KC_Y = 89;
		public static const KC_Z = 90;
		
		public static const KC_0 = 48;
		public static const KC_1 = 49;
		public static const KC_2 = 50;
		public static const KC_3 = 51;
		public static const KC_4 = 52;
		public static const KC_5 = 53;
		public static const KC_6 = 54;
		public static const KC_7 = 55;
		public static const KC_8 = 56;
		public static const KC_9 = 57;
			
		public static const KC_F1 = 112;
		public static const KC_F2 = 113;
		public static const KC_F3 = 114;
		public static const KC_F4 = 115;
		public static const KC_F5 = 116;
		public static const KC_F6 = 117;
		public static const KC_F7 = 118;
		public static const KC_F8 = 119;
		public static const KC_F9 = 120;
		public static const KC_F10 = 121;
		public static const KC_F11 = 122;
		public static const KC_F12 = 123;
	}

}