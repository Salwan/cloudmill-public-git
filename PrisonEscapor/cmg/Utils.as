package cmg 
{
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * Numorous utilities.
	 * @author ZenithSal
	 */
	public class Utils
	{
		public static const toDeg:Number = 57.2957;
		public static const toRad:Number = 0.01745;
		
		public function Utils() 
		{
			
		}
		
		public static function lerp(a:Number, b:Number, alpha:Number):Number
		{
			return a + ((b - a) * alpha)
		}
		
		public static function saturate(n:Number):Number
		{
			return n < 0.5? 0.0 : 1.0;
		}
		
		public static function cap(n:Number, max:Number, min:Number):Number
		{
			return (n > max? max : (n < min? min : n));
		}
		
		public static function randomInt(min:int, max:int):int
		{
			return int(Math.floor((Math.random() * Math.abs(max - min)) + min));
		}
		
		// A Boolean dice
		public static function chance(percent:Number = 0.5, total:Number = 1.0):Boolean
		{
			var dice:Number = Math.random() * total;
			var h:Number = Math.random();
			if (h >= 0.66) // High
			{
				if (dice >= (total - percent))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else if(h >= 0.33) // Low
			{
				if (dice <= percent)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else // Medium
			{
				var n:Number = (total / 2) - (percent / 2);
				if (dice >= n && dice <= n + percent)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		
	}

}