package cmg
{
	import flash.display.Stage;
	import flash.utils.Dictionary;
	
	/**
	 * Game world parameters and objects.
	 * @author ZenithSal
	 */
	public class World 
	{
		public static var theStage:Stage = null;
		public static var stageWidth:int = 800.0;
		public static var stageHeight:int = 500.0;
		public static var gravity:Number = 150.0;
		public static var layers:Dictionary = new Dictionary(true);
		public static var globals:Dictionary = new Dictionary(true);
		
		public function World() 
		{
			
		}
		
		
		public static function dump():void
		{
			trace("World Parameters");
			trace("- Stage Width:  " + World.stageWidth);
			trace("- Stage Height: " + World.stageHeight);
			trace("- Gravity:      " + World.gravity);
		}
	}

}