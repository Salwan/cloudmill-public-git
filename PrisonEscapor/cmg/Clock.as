package cmg
{
	import flash.utils.getTimer;
	
	/**
	 * Centarlized system clock
	 * @author ZenithSal
	 */
	public class Clock 
	{
		public static var deltaTime:Number = 0.0; 		// Can be paused (use for game stuff)
		public static var sysDeltaTime:Number = 0.0; 	// Cannot be paused (use for system stuff)
		
		protected var iPrevTime:int = 0;
		protected var isPaused:Boolean = false;
		protected var iPausedTime:int = 0;		
		protected var iSysPrevTime:int = 0;
		
		public function Clock() 
		{
			iPrevTime = getTimer();
			iSysPrevTime = iPrevTime;
		}
		
		public function tick()
		{
			var curTime:int = getTimer();

			// System clock
			var dt = curTime - iSysPrevTime;
			iSysPrevTime = curTime;
			Clock.sysDeltaTime = dt / 1000.0;
			
			// Game clock
			if (!isPaused)
			{				
				dt = curTime - iPrevTime;
				iPrevTime = curTime;
				Clock.deltaTime = dt / 1000.0;
			}
		}
		
		public function pause()
		{
			isPaused = true;
			iPausedTime = getTimer();
			deltaTime = 0.0;
		}
		
		public function resume()
		{
			isPaused = false;
			iPrevTime = iPausedTime;
		}
		
	}

}