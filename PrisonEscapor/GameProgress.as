package  
{
	/**
	 * Game wide data
	 * @author ZenithSal
	 */
	public class GameProgress 
	{
		public static var Round:int = 1;
		public static var RoundCount:int = 5;
		public static var Lives:int = 3;
		public static var Time:Array = new Array();
		public static var Debug:Boolean = false;
		
		public function GameProgress() 
		{
		}
		
		public static function newGame()
		{
			Round = 1;
			Lives = 3;
			Time = new Array();
		}
		
	}

}