﻿package  
{	
	import flash.display.MovieClip;	
	import flash.events.Event;
	import cmg.Clock;
	
	public class TunnelTile extends MovieClip 
	{
		protected const DRILL_RATE:Number = 5.0;
		protected static var sndDigTile:Sound_DigTile = null;
		protected var isDug:Boolean = false;
		
		public function TunnelTile() 
		{
			alpha = 0;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
		}
		
		public function drill():Boolean
		{
			if (!isDug)
			{
				alpha += DRILL_RATE * Clock.deltaTime;
				if (alpha >= 1.0)
				{
					isDug = true;
					sndDigTile = new Sound_DigTile();
					sndDigTile.play();
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return false;
			}
		}
		
		public function dug():Boolean
		{
			return isDug;
		}
	}
	
}
