﻿package  
{	
	import flash.display.MovieClip;	
	import flash.events.Event;
	import cmg.*;
	
	public class CPerformanceCounter extends MovieClip 
	{		
		public function CPerformanceCounter() 
		{
			mouseEnabled = false;
			mouseChildren = false;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
		}
		
		protected function onEnterFrame(e:Event):void
		{
			performFPS.text = Perform.ActualFrameRate + " (" + Perform.DefaultFrameRate + ")";
			performTimeDelta.text = Perform.TimeDelta.toString();
			
			if (Perform.PerformIndex >= 7)
			{
				performIndex.textColor = 0x22FF22;
			}
			else if (Perform.PerformIndex >= 4)
			{
				performIndex.textColor = 0xFFFF22;
			}
			else
			{
				performIndex.textColor = 0xFF2222;
			}
			performIndex.text = Perform.PerformIndex.toString() + " (" + Perform.AveragePerformIndex.toString() + ")";
			
			var mu:uint = Perform.MemoryUsage;
			var sufx:String = " bytes";			
			if (mu > 1024)
			{
				// In kilobytes	
				mu = mu / 1024.0;
				sufx = " KB";
			}			
			if (mu > 1024)
			{
				// In megabytes
				mu = mu / 1024.0;
				sufx = " MB";
			}			
			performMemory.text = mu.toString() + sufx;
		}
	}
	
}
