﻿package  
{	
	import cmg.Clock;
	import cmg.Reference;
	import cmg.StateMachine;
	import cmg.StateMachineEvent;
	import cmg.tweener.Tweener;
	import cmg.World;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import cmg.CollisionEvent;
	import cmg.Collisions;
	
	public class Guard extends Character
	{			
		protected var myRoom:Room = null;
		protected var stateMachine:StateMachine = null;
		protected var prevRoom:Room = null;
		
		// Motion state
		protected const STATE_IDLE:int = 0;
		protected const STATE_RUNTO:int = 1;
		
		protected const GUARD_SPEED:Number = 90.0;
		protected const ALERT_TIME:Number = 0.75;
		
		protected var targetTile:Point = null;
		protected var targetObject:Object = null;
		protected var motionState:int = STATE_IDLE;
		protected var traceIndex:int = -1;
		protected var waypointIndex:int = 0;
		
		public function Guard() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage, false, 0, true);
			
			id = "guard";
		}
		
		protected function onAddedToStage(e:Event):void
		{
			mcHitBox.alpha = 0;
			hitBox = Sprite(mcHitBox);
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			announceNewPosition();
			
			stateMachine = new StateMachine("Guard", true);
			stateMachine.addState("idle", { enter:onIdleEnter, update:onIdleUpdate, exit:onIdleExit, from:"*" } );
			stateMachine.addState("alert", { enter:onAlertEnter, update:onAlertUpdate, exit:onAlertExit, from:"*" } );
			stateMachine.addState("chase", { enter:onChaseEnter, update:onChaseUpdate, exit:onChaseExit, from:"alert" } );
			stateMachine.addState("aggressive", { enter:onAggrEnter, update:onAggrUpdate, exit:onAggrExit, from:"chase" } );
			stateMachine.addState("beating", { enter:onBeatingEnter, update:onBeatingUpdate, exit:onBeatingExit, from:"*" } );
			stateMachine.initialState = "idle";
			motionState = STATE_IDLE;
			
			Collisions.addElement("guards", this);
			addEventListener(CollisionEvent.ON_COLLISION, onCollision, false, 0, true);
			
			//hitBox = Sprite(mcHitBox);
		}
		
		protected function onRemovedFromStage(e:Event):void
		{
			if (traceIndex > -1)
			{
				CharacterManager.stopTracingMovement(traceIndex);
				traceIndex = -1;
			}
		}
		
		protected function onEnterFrame(e:Event):void
		{
			// precaution
			if (World.globals["game"].isWon())
			{
				return;
			}
			if (!myRoom)
			{
				myRoom = RoomManager.getRoomAt(x, y);
			}
			else
			{
				alpha = myRoom.alpha;
			}
			
			stateMachine.update();
			
			if (motionState == STATE_RUNTO)
			{
				// Update position
				doRunning();
			}
		}
		
		protected function onCollision(e:CollisionEvent):void
		{
			if (e.otherGroup == "prisoners" && targetObject == e.otherSprite)
			{
				if (Character(targetObject).id != "brawler") // police are afraid of brawler
				{
					stateMachine.changeState("beating");
				}
			}
			if (e.otherGroup == "player" && stateMachine.state == "chase" || stateMachine.state == "aggressive")
			{
				targetObject = e.otherSprite;
				stateMachine.changeState("beating");
			}
		}
		
		protected function runTo(p:Point)
		{
			targetTile = p;
			motionState = STATE_RUNTO;
		}
		
		protected function doRunning():void
		{
			if (motionState == STATE_RUNTO && targetTile)
			{
				// calculate new point
				var p:Point = new Point();
				p.x = (targetTile.x * 32) + 16;
				p.y = (targetTile.y * 32) + 16;
				
				
				var bx:Boolean = false;
				var by:Boolean = false;
				
				if (x < p.x)
				{
					x += GUARD_SPEED * Clock.deltaTime;
					if (x > p.x)
					{
						x = p.x;
						bx = true;
					}
				}
				else if (x > p.x)
				{
					x -= GUARD_SPEED * Clock.deltaTime;
					if (x < p.x)
					{
						bx = true;
						x = p.x;
					}
				}
				else 
				{
					bx = true;
				}
				if (y < p.y)
				{
					y += GUARD_SPEED * Clock.deltaTime;
					if (y > p.y)
					{
						y = p.y;
						by = true;
					}
				}
				else if (y > p.y)
				{
					y -= GUARD_SPEED * Clock.deltaTime;
					if (y < p.y)
					{
						y = p.y;
						by = true;
					}
				}
				else
				{
					by = true;
				}
				
				announceNewPosition(); // Keeps character manager aware of where this guard is
				
				if (bx && by)
				{
					onArrivedRunning();
				}
			}
			else
			{
				onArrivedRunning();
			}
		}
		
		protected function onArrivedRunning():void
		{
			motionState = STATE_IDLE;
			targetTile = null;
		}
		
		override public function onCharEnterRoom(char:Character):void 
		{
			super.onCharEnterRoom(char);
			
			if (char.id == "guard")
			{
				return;
			}
			
			// If guard is already doing something else, won't bother. 
			// they are just bad at multi-tasking
			if (stateMachine.state == "idle")
			{
				if (char.id == "escapor")
				{
					// it's you asshole! time to die
					targetObject = char;
					targetTile = new Point(char.x, char.y);
					goAlert();
				}
			}
		}
		
		override public function onCharExitRoom(char:Character):void 
		{
			super.onCharExitRoom(char);
		}
		
		override public function onRoomEnter(room:Room):void 
		{
			super.onRoomEnter(room);
			if (stateMachine)
			{
				if (stateMachine.state == "chase" && room != prevRoom)
				{
					// if guard is chasing, it's time to go aggressive
					stateMachine.changeState("aggressive");
					prevRoom = room;
				}
			}
		}
		
		override public function onRoomExit(room:Room):void 
		{
			super.onRoomExit(room);
			prevRoom = room;
		}
		
		protected function goAlert():void
		{
			stateMachine.changeState("alert");
		}
		
		protected function getMsgPoint():Point
		{
			var p:Point = new Point(mpMsg.x, mpMsg.y);
			return localToGlobal(p);
		}
		
		/////////////////////////////////////////////// Idle
		protected function onIdleEnter(e:StateMachineEvent):void
		{
			if (traceIndex > -1)
			{
				CharacterManager.stopTracingMovement(traceIndex);
				traceIndex = -1;
			}
		}
		
		protected function onIdleUpdate(e:StateMachineEvent):void
		{
		}
		
		protected function onIdleExit(e:StateMachineEvent):void
		{			
		}
		
		/////////////////////////////////////////////// Alert
		protected var alertTimer:Number = 0.0;
		protected function onAlertEnter(e:StateMachineEvent):void
		{
			var p:Point = new Point(mpMsg.x, mpMsg.y);
			p = localToGlobal(p);
			saySomething("STOP RIGHT THERE!", p);
			
			alertTimer = 0.0;
			
			traceIndex = CharacterManager.traceCharacterMovement(Character(targetObject));
		}
		
		protected function onAlertUpdate(e:StateMachineEvent):void
		{
			alertTimer += Clock.deltaTime;
			if (alertTimer >= ALERT_TIME)
			{
				stateMachine.changeState("chase");
			}
		}
		
		protected function onAlertExit(e:StateMachineEvent):void
		{			
		}
		
		/////////////////////////////////////////////// Chase
		protected function onChaseEnter(e:StateMachineEvent):void
		{
			var refp:Reference = new Reference();
			refp.value = new Point();
			waypointIndex = 0;
			waypointIndex = CharacterManager.getMovementWaypoint(traceIndex, waypointIndex, refp);
			if (waypointIndex != -1)
			{
				runTo(refp.value);
			}
			else
			{
				// This shouldnt happen but back to idle if it did
				stateMachine.changeState("idle");
			}
		}
		
		protected function onChaseUpdate(e:StateMachineEvent):void
		{
			if (motionState == STATE_IDLE && waypointIndex != -1) // waypointIndex != -1 so it doesn't loop for now
			{
				// Not running? get next movement point
				var refp:Reference = new Reference();
				refp.value = new Point();
				waypointIndex = CharacterManager.getMovementWaypoint(traceIndex, waypointIndex, refp);
				//trace("waypoint: ", waypointIndex);
				if (waypointIndex != -1)
				{
					//trace("Chasing Escapor to waypoint(", waypointIndex, "): ", refp.value.x,", ", refp.value.y);
					runTo(refp.value);
				}
			}
			
			if (waypointIndex == -1 && motionState == STATE_IDLE)
			{
				stateMachine.changeState("idle");		
				x = prevRoom.x;
				y = prevRoom.y;
			}
		}
		
		protected function onChaseExit(e:StateMachineEvent):void
		{			
			CharacterManager.stopTracingMovement(traceIndex);
			traceIndex = -1;
			waypointIndex = 0;
		}
		
		/////////////////////////////////////////////// Idle
		protected function onAggrEnter(e:StateMachineEvent):void
		{
			// Who is with me in this room?
			// -- CharacterManager answers
			// Is escapor here too?
			// Yes: chase him down
			// No: pick a random guy in the room, and chase him down (I'm not very bright)
			
			var foundSomeone:Boolean = false;
			var a:Array = CharacterManager.whoseHere(CharacterManager.whereAmI(this));
			if (a.length > 0)
			{
				for(var i in a)
				{
					if (a[i] is Escapor)
					{
						trace("ESCAPOR, you are mine!");
						targetObject = a[i];
						targetTile = new Point(Math.floor(a[i].x / 32), Math.floor(a[i].y / 32));
						stateMachine.changeState("alert");
						foundSomeone = true;
						break;
					}
					else if (a[i] is Prisoner)
					{
						trace("WHOEVER you are, dead!");
						targetObject = a[i];
						targetTile = new Point(Math.floor(a[i].x / 32), Math.floor(a[i].y / 32));
						stateMachine.changeState("alert");
						foundSomeone = true;
					}
				}
			}
			if(!foundSomeone)
			{
				saySomething("Where did he go?!", getMsgPoint());
				stateMachine.changeState("idle");
			}
		}
		
		protected function onAggrUpdate(e:StateMachineEvent):void
		{
		}
		
		protected function onAggrExit(e:StateMachineEvent):void
		{			
		}
		
		///////////////////////////////////////////////// Beating
		protected function onBeatingEnter(e:StateMachineEvent):void
		{
			var beat:BeatingEffect = new BeatingEffect(4.0);
			beat.x = x;
			beat.y = y - 32;
			TextLayer(World.layers["text"]).addChild(beat);
			visible = false;
			Character(targetObject).visible = false;
			Character(targetObject).kill();
			kill();			
		}
		
		protected function onBeatingUpdate(e:StateMachineEvent):void
		{
			
		}
		
		protected function onBeatingExit(e:StateMachineEvent):void
		{
			
		}
		
	}
	
}
