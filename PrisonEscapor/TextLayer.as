﻿package  
{	
	import flash.display.MovieClip;	
	import flash.events.Event;
	import cmg.World;
	import cmg.tweener.Tweener;
	
	public class TextLayer extends MovieClip 
	{
		protected var mcMsg:MovieClip = null;
		
		public function TextLayer() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			World.layers["text"] = this;
		}		
	}
	
}
