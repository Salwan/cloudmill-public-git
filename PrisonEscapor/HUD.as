﻿package  
{	
	import cmg.tweener.Tweener;
	import flash.display.MovieClip;
	import flash.events.Event;
	import cmg.Clock;
	import cmg.World;
	
	public class HUD extends MovieClip 
	{
		protected var time:Number = 0.0;
		protected var lifeHearts:Array;
		
		public function HUD() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void 
		{
			World.layers["hud"] = this;
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);

			lifeHearts = new Array();
			for (var i:int = 1; i <= GameProgress.Lives; ++i)
			{
				lifeHearts[i] = new LifeHeart();
				lifeHearts[i].x = 11 + (i * 38);
				lifeHearts[i].y = 20;
				addChild(lifeHearts[i]);
			}
		}
		
		protected function onEnterFrame(e:Event):void
		{
			time += Clock.deltaTime;
			
			var min:Number = Math.floor(time / 60.0);
			var sec:Number = Math.floor(time - (min * 60.0));
			var strMin:String;
			txtTime.text = addZero(min) + min.toString() + ":" + addZero(sec) + sec.toString();
			
			for (var i:int = 3; i >= 1; --i)
			{
				if (GameProgress.Lives < i && lifeHearts[i])
				{
					removeChild(lifeHearts[i]);
					lifeHearts[i] = null;
				}
			}
		}
		
		protected function addZero(t:Number):String
		{
			return t < 10? "0" : "";
		}
		
		public function getTime():Number
		{
			return time;
		}
	}
	
}
