﻿package  
{	
	import cmg.MovieClipEx;
	import flash.events.Event;
	
	public class SteelWall extends MovieClipEx 
	{
		public function SteelWall() 
		{
		}
		
		override protected function onFirstFrame():void 
		{
			TunnelManager.addSteelWall(this);
			
			if (GameProgress.Debug)
			{
				alpha = 0.2;
			}
			else
			{
				alpha = 0.0;
			}
		}
	}
	
}
