﻿package  {
	
	import cmg.tweener.Tweener;
	import flash.display.MovieClip;
	import flash.events.Event;
	import cmg.MovieClipEx;
	
	public class GameEnding extends MovieClipEx {
		
		
		public function GameEnding() {
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			mcFirst.alpha = 0;
			mcSecond.alpha = 0;
			mcThird.alpha = 0;
			
			Tweener.addTween(mcFirst, {alpha:1, time:0.5, delay:1, transition:"linear"});
			Tweener.addTween(mcSecond, {alpha:1, time:0.5, delay:2, transition:"linear"});
			Tweener.addTween(mcThird, { alpha:1, time:0.5, delay:3, transition:"linear" } );
			Tweener.addTween(mcThird, { alpha:1, time:0.5, delay:10, onComplete:onFinished } );
		}
		
		protected function onFinished():void
		{
			CMGGame(parent).changeScene("Main Menu");
		}
	}
	
}
