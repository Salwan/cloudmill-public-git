﻿package  
{	
	import flash.display.MovieClip;	
	import flash.events.Event;
	import cmg.tweener.Tweener;
	
	public class SaySomething extends MovieClip 
	{
		public function SaySomething() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			Tweener.addTween(this, { alpha:1, time:0.5, transition:"linear", onComplete:onMessageShow } );
		}
		
		protected function onMessageShow():void
		{
			Tweener.addTween(this, { alpha:0, time:0.5, delay:2.5, transition:"linear", onComplete:onMessageHide } );
		}
		
		protected function onMessageHide():void
		{
			parent.removeChild(this);
		}
	}
	
}
