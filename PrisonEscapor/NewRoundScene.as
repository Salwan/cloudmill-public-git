﻿package  
{	
	import cmg.tweener.Tweener;
	import flash.display.MovieClip;	
	import flash.events.Event;
	
	public class NewRoundScene extends MovieClip 
	{
		public function NewRoundScene() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			trace("NEW ROUND SCREEN");
			gotoAndStop(1);
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			txtMsg.text = "PRISON " + GameProgress.Round.toString();
			alpha = 0;
			Tweener.addTween(this, { alpha:1, time:0.5, transition:"linear" } );
			Tweener.addTween(this, { alpha:0, time:0.5, delay:2, transition:"linear", onComplete:backToGame } );
		}
		
		protected function onEnterFrame(e:Event):void
		{
			
		}
		
		protected function backToGame():void
		{
			CMGGame(parent).changeScene("Game");
		}
	}
	
}
