﻿package  
{	
	import flash.display.MovieClip;	
	import flash.events.Event;
	
	public class LifeHeart extends MovieClip 
	{		
		
		public function LifeHeart() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			scaleX = 0.8;
			scaleY = 0.8;
		}
	}
	
}
