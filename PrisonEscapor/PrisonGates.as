﻿package  
{	
	import cmg.tweener.Tweener;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;	
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	public class PrisonGates extends MovieClip 
	{				
		protected var isOpen:Boolean = false;
		protected var sndGateOpen:Sound_GateOpen = null;
		
		public function PrisonGates() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			TunnelManager.setGates(this);
		}
		
		public function openGates():void
		{
			if (!isOpen)
			{
				isOpen = true;
				var t1:int = x - mcGateLeft.width;
				var t2:int = x + mcGateRight.width;
				Tweener.addTween(mcGateLeft, { time:3, x:t1, transition:"linear" } );
				Tweener.addTween(mcGateRight, { time:3, x:t2, transition:"linear" } );
				sndGateOpen = new Sound_GateOpen();
				sndGateOpen.play();
			}
		}
		
		public function gateOpen():Boolean
		{
			return isOpen;
		}
		
		public function getGateRect(target_coords:DisplayObject):Rectangle
		{
			return mcGateBox.getRect(target_coords);
		}
		
		public function getBox():MovieClip
		{
			return MovieClip(mcGateBox);
		}
	}
	
}
