package  
{
	import cmg.flintparticles.common.actions.Age;
	import cmg.flintparticles.common.actions.Fade;
	import cmg.World;
	import flash.geom.Point;  
	import cmg.flintparticles.common.counters.*;
	import cmg.flintparticles.common.displayObjects.RadialDot;
	import cmg.flintparticles.common.initializers.*;
	import cmg.flintparticles.twoD.actions.*;
	import cmg.flintparticles.twoD.emitters.Emitter2D;
	import cmg.flintparticles.twoD.initializers.*;
	import cmg.flintparticles.twoD.zones.*;
	import cmg.flintparticles.common.counters.Random;
	
	/**
	 * ...
	 * @author ZenithSal
	 */
	public class FlintBeatingStarsEmitter extends Emitter2D
	{
		
		public function FlintBeatingStarsEmitter() 
		{
			//counter = new Blast(14);
			counter = new Random(3, 5);
			
			// Initialize properties
			addInitializer(new ImageClass(BeatingStars));
			addInitializer(new Position(new PointZone()));
			//addInitializer(new Velocity(new LineZone(new Point( -30, -30), new Point(30, 30))));
			addInitializer(new Velocity(new DiscZone(new Point(0, 0), 40, 20)));
			addInitializer(new ScaleImageInit(0.5, 1.75));
			addInitializer(new Lifetime(2, 3));
			
			// Actions
			addAction(new Move());
			//addAction(new Accelerate(0, World.gravity));
			addAction(new DeathZone(new RectangleZone( -10, -10, cmg.World.stageWidth + 10, World.stageHeight + 10), true));
			addAction(new Age());
			addAction(new Fade());
		}
		
	}

}