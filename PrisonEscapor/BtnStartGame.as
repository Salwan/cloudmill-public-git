﻿package  
{	
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
		
	public class BtnStartGame extends SimpleButton 
	{
		public function BtnStartGame() 
		{
			addEventListener(MouseEvent.CLICK, onClickEx, false, 0, true);
		}
		
		protected function onClickEx(e:MouseEvent):void
		{
			MainMenu(parent).newGame();
		}
	}
	
}
