﻿package  {
	
	import adobe.utils.CustomActions;
	import flash.display.MovieClip;
	import flash.events.Event;
	
	
	public class OutsideRoom extends Room {
		
		
		public function OutsideRoom() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage2, false, 0, true);
		}
		
		protected function onAddedToStage2(e:Event):void
		{
			alpha = 0;
			visible = false;
		}
	}
	
}
