﻿package  
{	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	public class HelpScene extends MovieClip 
	{
		public function HelpScene() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			gotoAndStop(1);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownEx);
			addEventListener(MouseEvent.CLICK, onClickEx);
		}
		
		protected function onRemovedFromStage(e:Event):void
		{
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownEx);
		}
		
		protected function onKeyDownEx(e:KeyboardEvent):void
		{
			progressHelp();
		}
		
		protected function onClickEx(e:MouseEvent):void
		{
			progressHelp();
		}
		
		protected function progressHelp()
		{
			if (currentFrame < totalFrames)
			{
				gotoAndStop(currentFrame + 1);
			}
			else
			{
				CMGGame(parent).changeScene("NewRound");
			}
		}
	}
	
}
