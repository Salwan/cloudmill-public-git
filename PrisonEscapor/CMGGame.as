﻿package  
{
	import cmg.*;
	import flash.events.Event;
	import flash.display.MovieClip;
	
	
	public class CMGGame extends MovieClip 
	{
		protected var clock:Clock;
		protected var perform:Perform;
		
		public function CMGGame() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{			
			clock = new Clock();
			perform = new Perform(stage);
				
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
						
			// Setup World
			cmg.World.theStage = stage;
			cmg.World.stageWidth = 800;
			cmg.World.stageHeight = 500;
			cmg.World.gravity = 150.0;
			cmg.World.dump();
			
			gotoAndStop(1);
			
			stage.addEventListener(Event.ACTIVATE, onActivate);
			stage.addEventListener(Event.DEACTIVATE, onDeactivate);
		}
		
		protected function onEnterFrame(e:Event):void
		{
			clock.tick();
			perform.tick();
		}
		
		protected function onActivate(e:Event):void
		{
			trace("# GAME ACTIVATED #");
			clock.resume();
		}
		
		protected function onDeactivate(e:Event):void
		{
			trace("# GAME DEACTIVATED #");
			clock.pause();
		}
		
		override public function nextScene():void 
		{
			super.nextScene();
			gotoAndStop(1);
		}
		
		public function changeScene(scene_name:String):void
		{
			gotoAndStop(1, scene_name);
		}
	}
	
}
