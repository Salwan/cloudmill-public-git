﻿package  
{	
	import flash.display.MovieClip;	
	import cmg.Utils;
	import cmg.MovieClipEx;
	
	public class PlayerWonScreen extends MovieClipEx 
	{		
		protected var rewards:Vector.<String> = null;
		
		public function PlayerWonScreen() 
		{
			rewards = new Vector.<String>();
			rewards.push("You see a double rainbow outside");
			rewards.push("You befriend a unicorn");
			rewards.push("Due to a strange desease.. you discover you are the only man left on earth, with 3 billion women");
			rewards.push("You do a backflip, while snowboarding, after jumping off an airplane");
			rewards.push("You cure cancer");
			rewards.push("You win LD#48");
			rewards.push("Chuck Norris works for you");
		}
		
		override protected function onFirstFrame():void 
		{
			setReward();
		}
		
		public function setTime(time:Number):void
		{
			var min:Number = Math.floor(time / 60.0);
			var sec:Number = Math.floor(time - (min * 60.0));
			var strMin:String;
			txtTime.text = "ESCAPED IN " + addZero(min) + min.toString() + ":" + addZero(sec) + sec.toString();
		}
		
		protected function addZero(t:Number):String
		{
			return t < 10? "0" : "";
		}
		
		public function setReward()
		{
			var n:int = Utils.randomInt(0, rewards.length - 1);
			txtReward.text = rewards[n];
		}
	}
	
}
