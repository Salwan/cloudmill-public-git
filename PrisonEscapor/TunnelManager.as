package  
{	
	import cmg.World;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.display.MovieClip;
	
	/**
	 * ...
	 * @author ZenithSal
	 */
	public class TunnelManager 
	{
		// 800x600, with 55 - 550 in tunnels. that's 495.
		// each tunnel is 32: 25x15.5
		protected static var tunnelData:Vector.< Vector.<MovieClip> > = null; // rows(columns)
		protected static var tunnelWalls:Array = null;
		protected static var sndDrillSteel:Sound_DrillSteel = null;
		protected static var sndDrillGates:Sound_DrillGates = null;
		protected static var isInit:Boolean = false;
		protected static var prisonGates:PrisonGates = null;
		
		public function TunnelManager() 
		{
			init();
		}		
		
		protected function init():void
		{
			tunnelWalls = new Array();
			tunnelData = new Vector.< Vector.<MovieClip> >();
			for (var j:int = 0; j < 19; ++j)
			{
				var v:Vector.<MovieClip> = new Vector.<MovieClip>();
				tunnelData.push(v);
				for (var i:int = 0; i < 25; ++ i)
				{
					v.push(null);
				}
			}
			isInit = true;
		}
		
		public static function setGates(g:PrisonGates):void
		{
			prisonGates = g;
		}
		
		public static function isGateOpen():Boolean
		{
			if (prisonGates)
			{
				return prisonGates.gateOpen();
			}
			return false;
		}
		
		public static function isInGate(r:int, c:int):Boolean
		{
			if (prisonGates)
			{
				if (prisonGates.getBox().hitTestPoint((c * 32) + 16, (r * 32) + 16))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		}
		
		public static function drill(x:int, y:int):void
		{
			var c:int = Math.floor(x / 32);
			var r:int = Math.floor(y / 32);
			
			// quick fix
			if (c > 24 || r > 19)
			{
				return;
			}
			
			//trace("DRILLING ", r, ", ", c);
			if (isInGate(r, c))
			{
				if (!isGateOpen())
				{
					// Cant drill gates
					sndDrillGates = new Sound_DrillGates();
					sndDrillGates.play();
				}
				else
				{
					// Can leave gate
					return;
				}
			}
			else if (tunnelData[r][c] == null)
			{
				var t:TunnelTile = new TunnelTile();
				t.x = c * 32;
				t.y = r * 32;
				tunnelData[r][c] = t;
				World.layers["tiles"].addChild(t);
			}
			else if (tunnelData[r][c] is SteelTile)
			{
				tunnelData[r][c].drill();
				// Not allowed to drill
				sndDrillSteel = new Sound_DrillSteel();
				sndDrillSteel.play();
			}
			else
			{
				tunnelData[r][c].drill();
			}
		}
		
		public static function isDug(x:int, y:int):Boolean
		{
			var c:int = Math.floor(x / 32);
			var r:int = Math.floor(y / 32);
			
			if (isInGate(r, c) && isGateOpen())
			{
				return true;
			}
			else if (tunnelData[r][c] == null)
			{
				return false;
			}
			else if (tunnelData[r][c] is SteelTile)
			{
				return false;
			}
			else
			{
				return tunnelData[r][c].dug();
			}
		}
		
		public static function addSteelWall(wall:SteelWall):void
		{
			var rect:Rectangle = wall.getRect(wall.parent);
			// use 5 points, left, right, top, bottom, and center
			var pa:Array = new Array();
			pa.push(new Point(rect.left, rect.y));
			pa.push(new Point(rect.right, rect.y));
			pa.push(new Point(rect.x, rect.y));
			pa.push(new Point(rect.x, rect.top));
			pa.push(new Point(rect.x, rect.bottom));
			for each(var p in pa)
			{
				var c:int = Math.floor(p.x / 32);
				var r:int = Math.floor(p.y / 32);
				if (tunnelData[r][c] == null && r < 25 && c < 19)
				{
					var wtile:SteelTile = new SteelTile();
					wtile.x = c * 32;
					wtile.y = r * 32;
					World.layers["tiles"].addChild(wtile);
					tunnelData[r][c] = wtile;
					//tunnelData[r][c] = wall;
				}
				
				//trace("ADDED STEEL TILE TO: ", c, ", ", r);
			}
		}
		
		public static function openGates():void
		{
			if (prisonGates && !prisonGates.gateOpen())
			{
				prisonGates.openGates();
			}
		}
	}

}