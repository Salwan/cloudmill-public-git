package  
{
	import cmg.flintparticles.common.actions.Age;
	import cmg.flintparticles.common.actions.Fade;
	import cmg.World;
	import flash.geom.Point;  
	import cmg.flintparticles.common.counters.*;
	import cmg.flintparticles.common.displayObjects.RadialDot;
	import cmg.flintparticles.common.initializers.*;
	import cmg.flintparticles.twoD.actions.*;
	import cmg.flintparticles.twoD.emitters.Emitter2D;
	import cmg.flintparticles.twoD.initializers.*;
	import cmg.flintparticles.twoD.zones.*;
	import cmg.flintparticles.common.counters.Random;
	
	/**
	 * ...
	 * @author ZenithSal
	 */
	public class FlintDrillDebrisEmitter extends Emitter2D
	{
		
		public function FlintDrillDebrisEmitter() 
		{
			//counter = new Blast(14);
			counter = new Random(7, 20);
			
			// Initialize properties
			addInitializer(new ImageClass(FlintDrillDebris_Image));
			addInitializer(new Position(new PointZone()));
			addInitializer(new Velocity(new LineZone(new Point( -50, -40), new Point(50, -80))));
			addInitializer(new ScaleImageInit(0.25, 0.75));
			addInitializer(new Lifetime(1, 1));
			
			// Actions
			addAction(new Move());
			addAction(new Accelerate(0, World.gravity));
			addAction(new DeathZone(new RectangleZone( -10, -10, cmg.World.stageWidth + 10, World.stageHeight + 10), true));
			addAction(new Age());
			addAction(new Fade());
		}
		
	}

}