﻿package  
{	
	import cmg.Keys;
	import flash.display.MovieClip;	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	public class MainMenu extends MovieClip 
	{
		public function MainMenu() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			gotoAndStop(1);
			
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUpEx, false, 0, true);
		}
		
		protected function onRemovedFromStage(e:Event):void
		{
			stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUpEx);
		}
		
		protected function onKeyUpEx(e:KeyboardEvent):void
		{
			if (e.keyCode == Keys.KC_SPACE || e.keyCode == Keys.KC_ENTER)
			{
				newGame();
			}
		}
		
		public function newGame():void
		{
			GameProgress.newGame();
			CMGGame(parent).changeScene("Help");
		}
	}
	
}
