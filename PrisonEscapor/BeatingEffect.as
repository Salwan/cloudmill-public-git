﻿package  
{	
	import cmg.Clock;
	import cmg.flintparticles.twoD.renderers.DisplayObjectRenderer;
	import flash.display.MovieClip;	
	import flash.events.Event;
	import flash.events.FocusEvent;
	
	public class BeatingEffect extends MovieClip 
	{
		protected var nTime:Number;
		protected var frameTime:Number = 0.0;
		protected const FRAME_TIME:Number = 0.2;
		protected var animFrame:int = 1;
		protected var beatingStarsRenderer:DisplayObjectRenderer;
		protected var sndBeating:Sound_Beating = null;
		
		public function BeatingEffect(time:Number)
		{
			nTime = time;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			scaleX = 1.3;
			scaleY = 1.3;
			
			gotoAndStop(animFrame);
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			
			var b:FlintBeatingStarsEmitter = new FlintBeatingStarsEmitter();
			b.x = x;
			b.y = y;
			beatingStarsRenderer = new DisplayObjectRenderer();
			beatingStarsRenderer.addEmitter(b);
			b.start();
			parent.addChild(beatingStarsRenderer);			
			
			sndBeating = new Sound_Beating();
			sndBeating.play();
		}
		
		protected function onRemovedFromStage(e:Event):void
		{
			parent.removeChild(beatingStarsRenderer);
		}
		
		protected function onEnterFrame(e:Event):void
		{
			nTime -= Clock.deltaTime;
			if (nTime <= 0.0)
			{
				removeEventListener(Event.ENTER_FRAME, onEnterFrame, false);
				parent.removeChild(this);
				return;
			}
			frameTime += Clock.deltaTime;
			if (frameTime >= FRAME_TIME)
			{
				frameTime = 0.0;
				animFrame += 1;
				if (animFrame > totalFrames)
				{
					animFrame = 1;
				}
				gotoAndStop(animFrame);
			}
		}
	}
	
}
