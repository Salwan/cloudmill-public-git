package  
{
	import flash.display.MovieClip;
	import cmg.World;
	import cmg.MovieClipEx;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author ZenithSal
	 */
	public class Level extends MovieClipEx
	{
		
		public function Level() 
		{
			tag = "level";
			
			trace("LEVEL START: ", GameProgress.Round);
			// Refactoring to make levels
			World.layers["steel"] = this;
			World.layers["rooms"] = this;
			World.layers["tunnels"] = this;
			World.layers["level"] = this;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		protected function onAddedToStage(e:Event):void
		{
		}
		
	}

}