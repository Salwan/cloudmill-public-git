﻿package  
{	
	import cmg.Clock;
	import cmg.CollisionEvent;
	import cmg.Collisions;
	import cmg.Utils;
	import cmg.World;
	import cmg.tweener.Tweener;
	import flash.display.MovieClip;	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class BrawlerPrisoner extends Character 
	{
		
		protected var myRoom:Room = null;
		protected var motionTime:Number = 0.5;
		
		public function BrawlerPrisoner() 
		{
			id = "brawler";
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			mcHitBox.alpha = 0;
			hitBox = Sprite(mcHitBox);
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			announceNewPosition();
			
			Collisions.addElement("prisoners", this);
			addEventListener(CollisionEvent.ON_COLLISION, onCollision, false, 0, true);
			
			gotoAndStop(1);
		}
		
		protected function onEnterFrame(e:Event):void
		{
			motionTime -= Clock.deltaTime;
			if (motionTime <= 0)
			{
				motionTime = 0.5;
				gotoAndStop(Utils.randomInt(1, totalFrames));
			}
			if (!myRoom)
			{
				myRoom = RoomManager.getRoomAt(x, y);
			}
			else
			{
				alpha = myRoom.alpha;
			}
		}
		
		protected function onCollision(e:CollisionEvent):void
		{
			trace("COLLISION WITH BRAWLER");
			var beat:BeatingEffect = new BeatingEffect(4.0);
			beat.x = x;
			beat.y = y - 32;
			TextLayer(World.layers["text"]).addChild(beat);
			visible = false;
			Character(e.otherSprite).visible = false;
			Character(e.otherSprite).kill();
			kill();	
		}
	}
	
}
