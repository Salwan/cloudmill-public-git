﻿package  
{	
	import cmg.StateMachine;
	import cmg.StateMachineEvent;
	import cmg.Keys;
	import cmg.Clock;
	import cmg.Utils;
	import cmg.World;
	import flash.display.MovieClip;	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import cmg.flintparticles.twoD.renderers.*;
	import cmg.flintparticles.twoD.emitters.Emitter2D;
	import cmg.Collisions;
	import cmg.CollisionEvent;
	
	public class Escapor extends Character 
	{
		protected const RUN_SPEED:Number = 100.0;
		protected const FRAME_IDLE_FRONT:int = 1;
		protected const FRAME_IDLE_SIDE:int = 2;
		protected const FRAME_RUN_START:int = 3;
		protected const FRAME_RUN_END:int = 22;
		
		protected var stateMachine:StateMachine = null;		
		protected var leftArrow:Boolean = false;
		protected var rightArrow:Boolean = false;
		protected var upArrow:Boolean = false;
		protected var downArrow:Boolean = false;
		protected var digKey:Boolean = false;
		protected var mcDrill:DiggingTool = null;
		
		protected var flintDrillRender:DisplayObjectRenderer = null;
		
		public function Escapor() 
		{
			id = "escapor";
			tag = "player";
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		public function isDrilling():Boolean
		{
			if (mcDrill)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		protected function onAddedToStage(e:Event):void
		{
			mcHitBox.alpha = 0;
			hitBox = Sprite(mcHitBox);
			
			stateMachine = new StateMachine("Escapor", false);
			stateMachine.addState("idle", { enter:onIdleEnter, update:onIdleUpdate, exit:onIdleExit, from:"*" } );
			stateMachine.addState("run", { enter:onRunEnter, update:onRunUpdate, exit:onRunExit, from:"idle" } );
			stateMachine.initialState = "idle";
			gotoAndStop(1);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownEx, false, 0, true);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUpEx, false, 0, true);
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			
			// Get first room and place escapor in the middle
			//currentRoom = Level(World.layers["rooms"]).mcStartRoom;
			//x = currentRoom.x;
			//y = currentRoom.y;
			//currentRoom.findNow();
			//
			//announceNewPosition();
			
			Collisions.addElement("player", this);
			addEventListener(CollisionEvent.ON_COLLISION, onCollision, false, 0, true);
			
			//hitBox = Sprite(mcHitBox);
		}
		
		override protected function onFirstFrame():void 
		{
			// Get first room and place escapor in the middle
			currentRoom = World.layers["rooms"].mcStartRoom;
			x = currentRoom.x;
			y = currentRoom.y;
			currentRoom.findNow();
			
			announceNewPosition();
		}
		
		protected function onEnterFrame(e:Event):void
		{
			stateMachine.update();
			if (mcDrill)
			{
				mcDrill.x = mpDrill.x;
				mcDrill.y = mpDrill.y;
			}
		}
		
		protected function onCollision(e:CollisionEvent):void
		{
			//trace("COLLISION: ", this, " and ", e.otherSprite, " from ", e.otherGroup);
		}
		
		protected function onKeyDownEx(e:KeyboardEvent):void
		{
			if (e.keyCode == Keys.KC_UP)
			{
				onUpArrow(true);
			}
			
			if (e.keyCode == Keys.KC_DOWN)
			{
				onDownArrow(true);
			}
			
			if (e.keyCode == Keys.KC_LEFT)
			{
				onLeftArrow(true);
			}
			
			if (e.keyCode == Keys.KC_RIGHT)
			{
				onRightArrow(true);
			}
			
			if (e.keyCode == Keys.KC_SPACE || e.ctrlKey)
			{
				onDig(true);
			}
		}
		
		protected function onKeyUpEx(e:KeyboardEvent):void
		{
			if (e.keyCode == Keys.KC_UP)
			{
				onUpArrow(false);
			}
			
			if (e.keyCode == Keys.KC_DOWN)
			{
				onDownArrow(false);
			}
			
			if (e.keyCode == Keys.KC_LEFT)
			{
				onLeftArrow(false);
			}
			
			if (e.keyCode == Keys.KC_RIGHT)
			{
				onRightArrow(false);
			}
			
			if (e.keyCode == Keys.KC_SPACE || e.ctrlKey)
			{
				onDig(false);
			}
		}
		
		protected function onUpArrow(isDown:Boolean):void
		{
			upArrow = isDown;
			scaleX = -1;
			if (isDown)
			{				
				if (stateMachine.state != "run")
				{	
					stateMachine.changeState("run");
				}
			}
			else
			{
				if (noArrowPress())
				{
					gotoAndStop(FRAME_IDLE_FRONT);
					stateMachine.changeState("idle");
				}
			}
		}
		
		protected function onDownArrow(isDown:Boolean):void
		{
			downArrow = isDown;
			if (isDown)
			{
				scaleX = 1;
				if (stateMachine.state != "run")
				{	
					stateMachine.changeState("run");
				}
			}
			else
			{
				if (noArrowPress())
				{
					gotoAndStop(FRAME_IDLE_FRONT);
					scaleX = 1;
					stateMachine.changeState("idle");
				}
			}
		}
		
		protected function onLeftArrow(isDown:Boolean):void
		{
			leftArrow = isDown;
			if (isDown)
			{
				scaleX = -1;
				if (stateMachine.state != "run")
				{	
					stateMachine.changeState("run");
				}
			}
			else
			{
				if (noArrowPress())
				{
					gotoAndStop(FRAME_IDLE_SIDE);
					scaleX = -1;
					stateMachine.changeState("idle");
				}
			}
		}
		
		protected function onRightArrow(isDown:Boolean):void
		{
			rightArrow = isDown;
			if (isDown)
			{
				scaleX = 1;
				if (stateMachine.state != "run")
				{	
					stateMachine.changeState("run");
				}
			}
			else
			{
				if (noArrowPress())
				{
					gotoAndStop(FRAME_IDLE_SIDE);
					scaleX = 1;
					stateMachine.changeState("idle");
				}
			}
		}
		
		protected function onDig(isDown:Boolean):void
		{
			if (isDown && !mcDrill)
			{
				mcDrill = new DiggingTool();
				mcDrill.x = mpDrill.x;
				mcDrill.y = mpDrill.y;
				addChild(mcDrill);
			}
			else if(!isDown && mcDrill)
			{
				if (flintDrillRender && parent)
				{
					parent.removeChild(flintDrillRender);					
				}
				flintDrillRender = null;
				removeChild(mcDrill);
				mcDrill = null;
			}
		}
		
		protected function noArrowPress():Boolean
		{
			return !(upArrow || downArrow || leftArrow || rightArrow);
		}
		
		//////////////////////////////////////////////////// Idle
		protected function onIdleEnter(e:StateMachineEvent):void
		{
		}
		
		protected function onIdleUpdate(e:StateMachineEvent):void
		{
		}
		
		protected function onIdleExit(e:StateMachineEvent):void
		{			
		}

		
		///////////////////////////////////////////////////// Run
		protected function onRunEnter(e:StateMachineEvent):void
		{		
			gotoAndPlay(FRAME_RUN_START);
			addFrameScript(FRAME_RUN_END - 1, onRunLastFrame);
		}
		
		protected function onRunUpdate(e:StateMachineEvent):void
		{
			var otherRoom:Room = null;
			
			checkHeading();
			
			var xx = x;
			var yy = y;
			
			if (upArrow)
			{
				yy -= RUN_SPEED * Clock.deltaTime;
			}
			if (downArrow)
			{
				yy += RUN_SPEED * Clock.deltaTime;
			}
			if (leftArrow)
			{
				xx -= RUN_SPEED * Clock.deltaTime;
			}
			if (rightArrow)
			{
				xx += RUN_SPEED * Clock.deltaTime;
			}
			xx = Utils.cap(xx, 799, 0);
			yy = Utils.cap(yy, 599, 0);
			
			// WINNING!!!
			if ((xx == 799 && yy > 512) || (yy == 599))
			{
				//trace("YOU ESCAPED!");
				World.globals["game"].onWon();
				return;
			}
			
			otherRoom = RoomManager.getRoomAt(xx, yy);
			
			if (currentRoom.inFloor(xx, yy)) // In room
			{
				x = xx;
				y = yy;
				if (flintDrillRender)
				{
					parent.removeChild(flintDrillRender);
					flintDrillRender = null;
				}
			}
			else if (otherRoom) // Check if escapor now in another room
			{
				if (otherRoom != currentRoom)
				{
					currentRoom = otherRoom;
					if (!currentRoom.Found)
					{
						currentRoom.findNow();
					}
				}
				x = xx;
				y = yy;
				if (flintDrillRender)
				{
					parent.removeChild(flintDrillRender);
					flintDrillRender = null;
				}
			}
			else // It's on the edge of the room
			{
				// Check if in dug tunnel
				if (TunnelManager.isDug(xx, yy))
				{
					x = xx;
					y = yy;
				}
				
				// Not in tunnel?
				if (mcDrill) // drill is running?
				{
					// Calculate drill position
					var dx = xx;
					var dy = yy;
					if (upArrow)
					{
						dy -= 1;
					}
					if (downArrow)
					{
						dy += 1;
					}
					if (leftArrow)
					{
						dx -= 1;
					}
					if (rightArrow)
					{
						dx += 1;
					}
					TunnelManager.drill(dx, dy);
					
					if (!flintDrillRender)
					{	
						var emt:FlintDrillDebrisEmitter = new FlintDrillDebrisEmitter();
						emt.x = dx;
						emt.y = dy;
						flintDrillRender = new DisplayObjectRenderer();
						flintDrillRender.addEmitter(emt);
						parent.addChild(flintDrillRender);
						emt.start();
					}
					if(flintDrillRender)
					{
						var p:Point = new Point(mpDrill.x, mpDrill.y);
						p = localToGlobal(p);
						FlintDrillDebrisEmitter(flintDrillRender.emitters[0]).x = p.x;
						FlintDrillDebrisEmitter(flintDrillRender.emitters[0]).y = p.y;
					}
				}
				else
				{
					if (flintDrillRender)
					{
						parent.removeChild(flintDrillRender);
						flintDrillRender = null;
					}
				}
			}
			
			applyHardLimit();
			announceNewPosition();
		}
		
		protected function onRunExit(e:StateMachineEvent):void
		{
			addFrameScript(FRAME_RUN_END - 1, null);
		}
		
		protected function onRunLastFrame():void
		{
			gotoAndPlay(FRAME_RUN_START);
		}
		
		protected function checkHeading():void
		{
			if (scaleX == -1)
			{
				if (downArrow || rightArrow)
				{
					scaleX = 1;
				}
			}
			if (scaleX == 1)
			{
				if ((upArrow && !rightArrow) || leftArrow)
				{
					scaleX = -1;
				}
			}
		}
		
		protected function applyHardLimit():void
		{
			if (x > 800)
			{
				x = 800;
			}
			if (x < 0)
			{
				x = 0;
			}
			
			if (y > 600)
			{
				y = 600;
			}
			
			if (y < 55)
			{
				y = 55;
			}
		}
		
		override public function kill():void 
		{
			// Lost a life
			GameProgress.Lives -= 1;
			World.globals["game"].onLostLife();
			super.kill();
		}
	}
	
}
