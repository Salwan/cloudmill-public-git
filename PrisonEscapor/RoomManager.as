package  
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	import cmg.World;
	
	/**
	 * ...
	 * @author ZenithSal
	 */
	public class RoomManager 
	{
		protected static var roomList:Vector.<Object> = null;
		protected static var isInit:Boolean = false;
		protected static var terminalList:Array = null;
		
		public function RoomManager() 
		{
			init();
		}
		
		protected static function init()
		{
			roomList = new Vector.<Object>();
			terminalList = new Array();
			isInit = true;
		}
		
		public static function addTerminal(terminal:ControlTerminal):void
		{
			terminalList.push(terminal);
		}
		
		public static function onTerminalDestroyed(terminal:ControlTerminal):void
		{
			terminalList.splice(terminalList.indexOf(terminal), 1);
			if (terminalList.length < 1)
			{
				// Open gates
				trace("OPEN GATES");
				TunnelManager.openGates();
			}
		}
		
		public static function addRoom(room:Room):void
		{
			if (!isInit)
			{
				init();
			}
			
			var r:Rectangle = room.getRect(room.parent);
			var obj:Object = new Object();
			obj["rect"] = r;
			obj["room"] = room;
			roomList.push(obj);
		}
		
		public static function removeRoom(room:Room):void
		{
			var idx:int = -1;
			for (var i:int = 0; i < roomList.length; ++i)
			{
				if (roomList[i]["room"] == room)
				{
					idx = i;
					break;
				}
			}
			if (idx != -1)
			{
				roomList.splice(idx, 1);
			}
		}
		
		public static function removeAll():void
		{
			roomList = new Vector.<Room>();
		}
		
		public static function getRoomAt(x:int, y:int):Room
		{
			for (var i:int = 0; i < roomList.length; ++i)
			{
				if (roomList[i]["rect"].contains(x, y))
				{
					return roomList[i]["room"];
				}
			}
			return null;
		}
		
		public static function getRoomRect(x:int, y:int):Rectangle
		{
			for (var i:int = 0; i < roomList.length; ++i)
			{
				if (roomList[i]["rect"].contains(x, y))
				{
					return roomList[i]["rect"];
				}
			}
			return null;
		}
	}

}