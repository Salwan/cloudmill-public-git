package  
{
	import cmg.World;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.geom.Point;
	import cmg.tweener.Tweener;
	import cmg.MovieClipEx;
	
	/**
	 * Base class for all characters
	 * @author ZenithSal
	 */
	public class Character extends MovieClipEx
	{		
		public var id:String = "";
		
		protected var _currentRoom:Room = null;
		
		public function Character() 
		{
			addEventListener(Event.ADDED_TO_STAGE, __onAddedToStage, false, 0, true);
		}
		
		protected function __onAddedToStage(e:Event):void
		{
			CharacterManager.addCharacter(this);
			currentRoom = RoomManager.getRoomAt(x, y);
		}		
		
		protected function set currentRoom(room:Room):void
		{
			if (room && _currentRoom != room)
			{
				// Room changed
				_currentRoom = room;
			}
		}
		
		protected function get currentRoom():Room
		{
			return _currentRoom;
		}
		
		protected function announceNewPosition():void
		{
			CharacterManager.announceNewPosition(this);
		}
		
		public function onCharEnterRoom(char:Character):void
		{
			
		}
		
		public function onCharExitRoom(char:Character):void
		{
			
		}
		
		public function onRoomEnter(room:Room):void
		{
			
		}
		
		public function onRoomExit(room:Room):void
		{
			
		}
		
		protected function saySomething(text:String, where:Point = null):void
		{
			var mcMessage:SaySomething = new SaySomething();
			if (where)
			{
				mcMessage.x = where.x;
				mcMessage.y = where.y;
			}
			mcMessage.txtMsg.text = text;
			TextLayer(World.layers["text"]).addChild(mcMessage);
		}	
		
		protected function showQuestionMark(where:Point = null):void
		{
			var questionMark:QuestionMark = new QuestionMark();
			if (where)
			{
				questionMark.x = where.x;
				questionMark.y = where.y;
			}
			TextLayer(World.layers["text"]).addChild(questionMark);
		}
		
		override public function kill():void
		{
			CharacterManager.onDead(this);
			super.kill();
		}
	}

}