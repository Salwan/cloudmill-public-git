﻿package  
{	
	import cmg.tweener.Tweener;
	import flash.display.MovieClip;	
	import flash.events.Event;
	import flash.geom.Rectangle;
	import cmg.World;
	
	public class Room extends MovieClip 
	{
		public const guardFrame:int = 0;
		
		protected var rect:Rectangle = null;
		protected var floorRect:Rectangle = null;
		protected var isFound:Boolean = false;
		
		public function Room() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			if (!isFound)
			{
				if (GameProgress.Debug)
				{
					alpha = 0.2;
				}
				else
				{
					alpha = 0;
				}
			}
			RoomManager.addRoom(this);
			rect = getRect(parent);
			floorRect = new Rectangle(rect.x + guardFrame, rect.y + guardFrame, 
				rect.width - (guardFrame * 2), rect.height - (guardFrame * 2));				
		}
		
		public function inFloor(_x:int, _y:int):Boolean
		{
			return floorRect.contains(_x, _y);
		}
		
		public function inFloorRect(r:Rectangle):Boolean
		{
			return floorRect.containsRect(r);
		}
		
		public function getRoomRect():Rectangle
		{
			return rect;
		}
		
		public function findNow():void
		{
			Tweener.addTween(this, { alpha:1, time:0.5, transition:"linear" } );
			isFound = true;
		}
		
		public function get Found():Boolean
		{
			return isFound;
		}
	}
	
}
