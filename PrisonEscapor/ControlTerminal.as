﻿package  
{	
	import cmg.Clock;
	import cmg.CollisionEvent;
	import cmg.Collisions;
	import flash.display.MovieClip;	
	import flash.events.Event;
	
	public class ControlTerminal extends MovieClip 
	{
		protected var isDestroyed:Boolean = false;
		protected var terminalHealth:Number = 2.0;
		protected var sndTerminalDrill:Sound_DrillGates = null;
		protected var sndTerminalExplode:Sound_TerminalExplode = null;
		
		public function ControlTerminal() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			gotoAndStop(1);
			Collisions.addElement("terminals", this);
			addEventListener(CollisionEvent.ON_COLLISION, onCollision, false, 0, true);
			RoomManager.addTerminal(this);
		}
		
		protected function onCollision(e:CollisionEvent):void
		{
			if(e.otherGroup == "player" && !isDestroyed)
			{
				if (Escapor(e.otherSprite).isDrilling())
				{
					sndTerminalDrill = new Sound_DrillGates();
					sndTerminalDrill.play();
					terminalHealth -= Clock.deltaTime;
					if (terminalHealth <= 0.0)
					{
						sndTerminalExplode = new Sound_TerminalExplode();
						sndTerminalDrill.play();
						trace("CONTROL TERMINAL DESTROYED");
						isDestroyed = true;
						gotoAndStop(2);
						RoomManager.onTerminalDestroyed(this);
					}
				}
			}
		}
	}
	
}
