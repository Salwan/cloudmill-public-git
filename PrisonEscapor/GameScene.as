﻿package  
{	
	import cmg.Clock;
	import cmg.Collisions;
	import cmg.flintparticles.common.behaviours.Behaviour;
	import cmg.tweener.Tweener;
	import cmg.Utils;
	import cmg.World;
	import flash.display.MovieClip;	
	import flash.events.Event;
	
	public class GameScene extends MovieClip 
	{
		protected var roomMgr:RoomManager = new RoomManager();
		protected var tunnelMgr:TunnelManager = new TunnelManager();
		protected var charMgr:CharacterManager = new CharacterManager();
		protected var collisionSystem:Collisions = new Collisions();
		
		protected const STATE_NORMAL:int = 0;
		protected const STATE_LOSTLIFE:int = 1;
		protected const STATE_LOSTGAME:int = 2;
		protected const STATE_WON:int = 3;
		
		protected var gameState:int = STATE_NORMAL;
		protected var lostLifeTimer:Number = 2.0;
		protected var lostGameTimer:Number = 3.0;

		
		public function GameScene() 
		{
			World.globals["game"] = this;
			
			// Define collision groups and relations before anything else
			Collisions.registerGroup("player");
			Collisions.registerGroup("guards");
			Collisions.registerGroup("prisoners");			
			Collisions.registerGroup("terminals");
			Collisions.setCollision("player", "guards");
			Collisions.setCollision("guards", "player");
			Collisions.setCollision("guards", "prisoners");
			Collisions.setCollision("prisoners", "guards");
			Collisions.setCollision("player", "prisoners");
			Collisions.setCollision("player", "terminals");
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			gotoAndStop(1);
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			
			
			// Create level
			var mcLevel:Level = null;
			switch(GameProgress.Round)
			{
				case 1:
					mcLevel = new Level1();
					break;
					
				case 2:
					mcLevel = new Level2();
					break;
					
				case 3:
					mcLevel = new Level3();
					break;
					
				case 4:
					mcLevel = new Level4();
					break;
					
				case 5:
					mcLevel = new Level5();
					break;
					
				default:
					trace("OUT OF LEVELS");
					mcLevel = new Level1();
			}
			addChildAt(mcLevel, getChildIndex(mcLevelDepth));
		}
		
		protected function onEnterFrame(e:Event):void
		{
			collisionSystem.process();
		}
		
		// I'm too tired to find a name
		protected function onLostLifeNow():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			CMGGame(parent).changeScene("NewRound");
		}
		
		protected function onLostGame():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			CMGGame(parent).changeScene("Main Menu");
		}
		
		protected function onPlayerWonNow():void
		{
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			GameProgress.Round += 1;
			if (GameProgress.Round <= GameProgress.RoundCount)
			{
				trace("NEXT ROUND");
				CMGGame(parent).changeScene("NewRound");
			}
			else
			{
				trace("GAME FINISHED");
				CMGGame(parent).changeScene("GameFinished");
			}
		}
		
		public function onLostLife():void
		{
			if (GameProgress.Lives >= 1)
			{
				Tweener.addTween(mcPlayerDied, { alpha:1, time:0.5, delay:1, transition:"linear" } );
				Tweener.addTween(mcPlayerDied, { time:0.5, delay:3, transition:"linear", onComplete:onLostLifeNow } );
				gameState = STATE_LOSTLIFE;
			}
			else
			{
				Tweener.addTween(mcGameOver, { alpha:1, time:0.5, delay:1, transition:"linear" } );
				Tweener.addTween(mcGameOver, { time:0.5, delay:3.5, transition:"linear", onComplete:onLostGame } );
				gameState = STATE_LOSTGAME;
			}
		}
		
		
		public function onWon():void
		{
			if (gameState != STATE_WON)
			{
				GameProgress.Time[GameProgress.Round] = World.layers["hud"].getTime();
				Tweener.addTween(mcPlayerWon, { alpha:1, time:0.5, delay:0.5, transition:"linear" } );
				Tweener.addTween(mcPlayerWon, { time:0.5, delay:5, transition:"linear", onComplete:onPlayerWonNow } );
				PlayerWonScreen(mcPlayerWon).setTime(World.layers["hud"].getTime());
				gameState = STATE_WON;
			}
		}
		
		public function isWon():Boolean
		{
			return gameState == STATE_WON? true : false;
		}
	}
	
}
