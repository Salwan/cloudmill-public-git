﻿package  
{	
	import cmg.CollisionEvent;
	import cmg.Collisions;
	import cmg.tweener.Tweener;
	import flash.display.MovieClip;	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	public class Prisoner extends Character
	{
		protected var myRoom:Room = null;
		protected var message:SaySomething = null;
		
		public function Prisoner() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			mcHitBox.alpha = 0;
			hitBox = Sprite(mcHitBox);
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame, false, 0, true);
			announceNewPosition();
			
			Collisions.addElement("prisoners", this);
			addEventListener(CollisionEvent.ON_COLLISION, onCollision, false, 0, true);
			
			//hitBox = Sprite(mcHitBox);
		}
		
		protected function onEnterFrame(e:Event):void
		{
			if (!myRoom)
			{
				myRoom = RoomManager.getRoomAt(x, y);
			}
			else
			{
				alpha = myRoom.alpha;
			}
		}
		
		protected function onCollision(e:CollisionEvent):void
		{
			//trace("COLLISION: ", this, " and ", e.otherSprite, " from ", e.otherGroup);
		}
		
		
		override public function onCharEnterRoom(char:Character):void 
		{
			if (char.id == "escapor")
			{
				super.onCharEnterRoom(char);
				var p:Point = new Point(mpMsg.x, mpMsg.y);
				p = localToGlobal(p);
				//saySomething("Get out of my cell", p);
				showQuestionMark(p);
			}
		}
	}
	
}
