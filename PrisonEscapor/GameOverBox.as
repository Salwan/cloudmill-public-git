﻿package  
{	
	import flash.display.MovieClip;	
	import flash.events.Event;
	
	public class GameOverBox extends MovieClip 
	{
		public function GameOverBox() 
		{
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
		}
		
		protected function onAddedToStage(e:Event):void
		{
			alpha = 0;
		}
	}
	
}
