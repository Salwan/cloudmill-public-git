#pragma once
#include <boost/shared_ptr.hpp>
#include "d3dutil.h"
#include "d3diautil.h"
#include "d3dresutil.h"
#include "d3dfxutil.h"
#include "pugixml\pugixml.hpp"

// ============= Mesh and parser for Collada ==============
class Mesh
{
public:
	Mesh(ID3D10Device* device, const std::wstring& model_file, const std::wstring& effect_file, float scale = 1.0f);
	virtual ~Mesh();

public:
	void draw(const char* tech_name);
	void setMatrix(const char* name, const D3DXMATRIX& matrix);
	FxUtil::Effect* getEffect() { return mEffect.get(); }

private:
	void parseColladaFile(const std::wstring& model_file);
	void parseGeometry(const pugi::xml_node& geometry_node);
	void createEffect(const std::wstring& effect_file);

private:
	ID3D10Device*										mDevice;
	boost::shared_ptr<IAUtil::VertexElementDesc>		mElementDesc;
	boost::shared_ptr<ResUtil::VertexBuffer<PNTVertex> > mVB;
	boost::shared_ptr<ResUtil::IndexBufferTriangleList> mIB;
	boost::shared_ptr<FxUtil::Effect>					mEffect;
	float												mScale;
};

