#pragma once
#include <cstdio>
#include <cstdarg>
#include <cstdlib>
#include <D3D10.h>
#include <D3DX10.h>
#include <DxErr.h>
#include <cassert>

#define TORADCONST 0.017453f
#define TODEGCONST 57.295779f

#define RELEASECOM(o) if(o) { o->Release(); o = NULL;}

#if defined(DEBUG) | defined(_DEBUG)
    #ifndef HR
    #define HR(x)                                                \
    {                                                            \
        HRESULT hr = (x);                                        \
        if(FAILED(hr))                                           \
        {                                                        \
            DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, true);   \
        }                                                        \
    }
    #endif
#else
    #ifndef HR
    #define HR(x) (x)
    #endif
#endif

const D3DXCOLOR WHITE(1.0f, 1.0f, 1.0f, 1.0f);
const D3DXCOLOR BLACK(0.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR RED(1.0f, 0.0f, 0.0f, 1.0f);
const D3DXCOLOR GREEN(0.0f, 1.0f, 0.0f, 1.0f);
const D3DXCOLOR BLUE(0.0f, 0.0f, 1.0f, 1.0f);
const D3DXCOLOR YELLOW(1.0f, 1.0f, 0.0f, 1.0f);
const D3DXCOLOR CYAN(0.0f, 1.0f, 1.0f, 1.0f);
const D3DXCOLOR MAGENTA(1.0f, 0.0f, 1.0f, 1.0f);

#define MAX_MESSAGE_LENGTH 1024

namespace Log
{
	static void message(const wchar_t* str, ...)
	{
		if(!str || wcsnlen_s(str, MAX_MESSAGE_LENGTH) == 0)
		{
			return;
		}
		wchar_t buffer[MAX_MESSAGE_LENGTH];
		va_list arguments;
		va_start(arguments, str);
		vswprintf_s(buffer, MAX_MESSAGE_LENGTH, str,arguments);
		OutputDebugString(buffer);
		va_end(arguments);
	}
}