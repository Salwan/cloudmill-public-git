#pragma once
#include "d3dutil.h"

struct Material
{
	D3DXVECTOR4 diffuse;
	D3DXVECTOR4 specular;
	float power;
};