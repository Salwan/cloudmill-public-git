///////////////////////////////////////////////////
// Per-Vertex Lighting shader
// Supports: Directional, Point, Spot lights
//
// TODO:
// - Ambients of all lights are added, ambient
//   should be global to the whole scene and not by
//   light. (it's not a light nor object property)
///////////////////////////////////////////////////

#define LIGHT_NONE			0
#define LIGHT_DIRECTIONAL	1
#define LIGHT_POINT			2
#define LIGHT_SPOT			3

struct Light
{
	float4 pos;
	float4 dir;
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 att;
	float range;
	float spotPower;
	float3 padding;
};

struct Material
{
	float4 diffuse;
	float4 specular;
	float power;
};

cbuffer cbPerFrame
{
	float3 gEyePos;
};

cbuffer cbPerObject
{
	float4x4 gWVP;
	Material gMaterial;
};

cbuffer cbPerScene
{
	Light gLight [4];
	vector<int, 4> gLightType;
	float4 gAmbient;
};

struct VS_IN
{
	float3 posL				: POSITION;
	float3 normal			: NORMAL;
	float2 tex				: TEXCOORD0;
};

struct VS_OUT
{
	float4 posH				: SV_POSITION;
	float4 color			: COLOR;
};

float4 lightDirectional(in Light light, float3 posL, float3 normal)
{
	float3 dir = light.dir.xyz;

	// Diffuse term
	float kd = max(dot(-dir, normal), 0.0f);

	// Specular term
	float3 v = normalize(gEyePos - posL);
	float3 r = reflect(dir, normal);
	float ks = pow(saturate(kd) * max(dot(v, r), 0.0f), gMaterial.power);

	return (light.ambient) + ((light.diffuse * gMaterial.diffuse) * kd) + ((light.specular * gMaterial.specular) * ks);
}

float4 lightPoint(in Light light, float3 posL, float3 normal)
{
	float3 dir = normalize(light.pos - posL);
	float d = distance(light.pos, posL);

	[branch]
	if(d < light.range)
	{
		// Diffuse term
		float kd = max(dot(dir, normal), 0.0f);

		// Specular term
		float3 v = normalize(gEyePos - posL);
		float3 r = reflect(-dir, normal);
		float ks = pow(saturate(kd) * max(dot(v, r), 0.0f), gMaterial.power);

		return ((light.ambient) + ((light.diffuse * gMaterial.diffuse) * kd) + ((light.specular * gMaterial.specular) * ks)) / (dot(light.att, float3(1.0f, d, d * d)));
	}
	else
	{
		return float4(0.0f, 0.0f, 0.0f, 0.0f);
	}
}

float4 lightSpot(in Light light, float3 posL, float3 normal)
{
	float3 dir = normalize(light.pos - posL);
	float d = distance(light.pos, posL);

	[branch]
	if(d < light.range)
	{
		// Diffuse term
		float kd = max(dot(dir, normal), 0.0f);
		float skd = saturate(kd);

		// Specular term
		float3 v = normalize(gEyePos - posL);
		float3 r = reflect(-dir, normal);
		float ks = pow(skd * max(dot(v, r), 0.0f), gMaterial.power);
		
		// Spot factor
		float kspot = pow(max(dot(-dir, light.dir), 0.0f), light.spotPower) * skd;

		return kspot * ((light.ambient) + ((light.diffuse * gMaterial.diffuse) * kd) + ((light.specular * gMaterial.specular) * ks)) / (dot(light.att, float3(1.0f, d, d * d)));
	}
	else
	{
		return float4(0.0f, 0.0f, 0.0f, 0.0f);
	}
}

VS_OUT LightVS(VS_IN input)
{
	VS_OUT output;
	output.posH = mul(float4(input.posL, 1.0f), gWVP);	
	output.color = gAmbient;

	[unroll]
	for(int i = 0; i < 4; ++i)
	{
		switch(gLightType[i])
		{
		case LIGHT_NONE:
			break;

		case LIGHT_DIRECTIONAL:
			{
				output.color += lightDirectional(gLight[i], input.posL, input.normal);
			}
			break;

		case LIGHT_POINT:
			{
				output.color += lightPoint(gLight[i], input.posL, input.normal);
			}
			break;

		case LIGHT_SPOT:
			{
				output.color += lightSpot(gLight[i], input.posL, input.normal);
				output.color = float4(0.0f, 0.0f, 1.0f, 0.0f);
			}
			break;
		}
	}
	return output;
}

float4 LightPS(				float4 posH		: SV_POSITION,
							float4 color	: COLOR) : SV_TARGET
{
	return color;
}

technique10 LightTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, LightVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, LightPS()));
	}
}