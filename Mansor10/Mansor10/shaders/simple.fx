cbuffer cbPerObject
{
	float4x4 gWVP;
};

struct VS_IN
{
	float3 posL				: POSITION;
	float4 color			: COLOR;
};

struct VS_OUT
{
	float4 posH				: SV_POSITION;
	float4 color			: COLOR;
};

VS_OUT SimpleVertexShader(VS_IN input)
{
	VS_OUT output;
	// Transform to homogeneous clip space
	output.posH = mul(float4(input.posL, 1.0f), gWVP);
	// Just pass vertex color into the pixel shader
	output.color = input.color;
	return output;
}

float4 SimplePixelShader(	float4 posH		: SV_POSITION,
							float4 color	: COLOR) : SV_TARGET
{
	return color;
}

technique10 SimpleTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, SimpleVertexShader()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, SimplePixelShader()));
	}
}