#pragma once

#include <D3D10.h>
#include <D3DX10.h>
#include <vector>
#include <cassert>
#include "d3dutil.h"

namespace ResUtil
{
	// Wraps a vertex buffer
	template <class VertexType>
	class VertexBuffer
	{
	public:
		VertexBuffer(ID3D10Device* device, unsigned int vertex_count, D3D10_USAGE usage = D3D10_USAGE_DEFAULT) 
			: mVB(NULL), mDevice(device), mVertexCount(vertex_count), mUsage(usage)
		{
			assert(mDevice);
			assert(mVertexCount);
		}

		virtual ~VertexBuffer()
		{
			RELEASECOM(mVB);
		}

	public:
		ID3D10Buffer* getBuffer()
		{
			if(!mVB)
			{
				createBuffer();
			}
			return mVB;
		}

		unsigned int getCount() const
		{
			return mVertexCount;
		}

		void setVertexBuffer(unsigned int offset = 0)
		{
			assert(mVB);

			unsigned int stride = sizeof(VertexType);
			mDevice->IASetVertexBuffers(0, 1, &mVB, &stride, &offset);
		}

		void setDrawVertexBuffer(unsigned int vertex_count, unsigned int start_location, 
			D3D10_PRIMITIVE_TOPOLOGY topology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
		{
			setVertexBuffer();
			D3D10_PRIMITIVE_TOPOLOGY currentTopology;
			mDevice->IAGetPrimitiveTopology(&currentTopology);
			if(currentTopology != topology)
			{
				mDevice->IASetPrimitiveTopology(topology);
			}
			mDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			mDevice->Draw(vertex_count, start_location);
		}

		void addVertex(VertexType& vertex)
		{
			assert(mVB == NULL);
			mVertices.push_back(vertex);
		}

		void addVertices(VertexType* vertices, unsigned int count)
		{
			assert(mVB == NULL);
			for(unsigned int i = 0; i < count; ++i)
			{
				mVertices.push_back(vertices[i]);
			}
		}

		void createBuffer()
		{
			assert(mVB == NULL); // Shouldn't create VB twice!
			assert(mVertices.size());

			UINT cpuFlags = 0;
			if(mUsage == ::D3D10_USAGE_DYNAMIC)
			{
				cpuFlags = ::D3D10_CPU_ACCESS_WRITE;
			}

			D3D10_BUFFER_DESC d;
			d.Usage = mUsage;
			d.ByteWidth = sizeof(VertexType) * mVertexCount;
			d.BindFlags = D3D10_BIND_VERTEX_BUFFER;
			d.CPUAccessFlags = cpuFlags;
			d.MiscFlags = 0;

			D3D10_SUBRESOURCE_DATA data;
			VertexType* vertexData = getVerticesPointer();
			data.pSysMem = vertexData;

			HR(mDevice->CreateBuffer(&d, &data, &mVB));

			delete vertexData;
			mVertices.clear();
		}

		void map(void** out_data)
		{
			assert(mUsage == D3D10_USAGE_DYNAMIC);
			mVB->Map(D3D10_MAP_WRITE_DISCARD, 0, out_data);
		}

		void unmap()
		{
			mVB->Unmap();
		}

	private:
		VertexType* getVerticesPointer()
		{
			// TODO: What should happen if you create the pointer with a number of vertices less than max?
			// for now, just crash. (on debug)
			assert(mVertices.size() >= mVertexCount);

			VertexType* buffer = new VertexType [mVertexCount];
			for(unsigned int i = 0; i < mVertices.size(); ++i)
			{
				if(i >= mVertexCount)
				{
					break;
				}
				buffer[i] = mVertices[i];
			}

			return buffer;
		}

	private:
		ID3D10Device*				mDevice;
		unsigned int				mVertexCount;
		D3D10_USAGE					mUsage;
		ID3D10Buffer*				mVB;
		std::vector<VertexType>		mVertices;
	};
	//------------------------------------------------------------------------------------

	// Wraps a general index buffer
	class IndexBuffer
	{
	public:
		IndexBuffer(ID3D10Device* device, unsigned int index_count, D3D10_USAGE usage = D3D10_USAGE_DEFAULT) 
			: mIB(NULL), mDevice(device), mIndexCount(index_count), mUsage(usage)
		{
			assert(mDevice);
			assert(mIndexCount);
		}
		virtual ~IndexBuffer()
		{
			RELEASECOM(mIB);
		}

	public:
		ID3D10Buffer* getBuffer()
		{
			if(!mIB)
			{
				createBuffer();
			}
			return mIB;
		}

		unsigned int getCount() const
		{
			return mIndexCount;
		}

		void setIndexBuffer(unsigned int offset = 0)
		{
			assert(mIB);
			mDevice->IASetIndexBuffer(mIB, DXGI_FORMAT_R32_UINT, offset);
		}

		void createBuffer()
		{
			assert(mIB == NULL); // Shouldn't create IB twice!
			assert(mIndices.size());

			D3D10_BUFFER_DESC d;
			d.Usage = mUsage;
			d.ByteWidth = sizeof(DWORD) * mIndexCount;
			d.BindFlags = D3D10_BIND_INDEX_BUFFER;
			d.CPUAccessFlags = 0;
			d.MiscFlags = 0;

			D3D10_SUBRESOURCE_DATA data;
			DWORD* indices = getIndicesPointer();
			data.pSysMem = indices;

			HR(mDevice->CreateBuffer(&d, &data, &mIB));

			delete [] indices;
			mIndices.clear();
		}

		DWORD* getIndicesPointer()
		{
			// TODO: What should happen if you create the pointer with a number of indices less than max?
			// for now, just crash. (on debug)
			assert(mIndices.size() >= mIndexCount);

			DWORD* buffer = new DWORD [mIndexCount];
			for(unsigned int i = 0; i < mIndices.size(); ++i)
			{
				if(i >= mIndexCount)
				{
					break;
				}
				buffer[i] = mIndices[i];
			}

			return buffer;
		}

		void addIndex(DWORD index)
		{
			assert(mIB == NULL);
			mIndices.push_back(index);
		}

		void addIndices(DWORD* indices, unsigned int count)
		{
			assert(mIB == NULL);
			for(unsigned int i = 0; i < count; ++i)
			{
				mIndices.push_back(indices[i]);
			}
		}

	protected:
		ID3D10Device*				mDevice;
		ID3D10Buffer*				mIB;
		unsigned int				mIndexCount;
		D3D10_USAGE					mUsage;
		std::vector<DWORD>			mIndices;
	};

	// Specifically wraps a triangle list index buffer.
	class IndexBufferTriangleList : public IndexBuffer
	{
	public:
		IndexBufferTriangleList(ID3D10Device* device, unsigned int index_count, D3D10_USAGE usage = D3D10_USAGE_DEFAULT) 
			: IndexBuffer(device, index_count, usage)
		{
		}
		virtual ~IndexBufferTriangleList()
		{
		}

	public:
		void addTriangle(DWORD v0, DWORD v1, DWORD v2)
		{
			mIndices.push_back(v0);
			mIndices.push_back(v1);
			mIndices.push_back(v2);
		}

		void addTriangle(DWORD* indices)
		{
			mIndices.push_back(indices[0]);
			mIndices.push_back(indices[1]);
			mIndices.push_back(indices[2]);
		}

		template <typename T>
		void setDrawIndexBuffer(unsigned int index_count, unsigned int start_location, 
			VertexBuffer<T>* vb, unsigned int base_vertex_location)
		{
			vb->setVertexBuffer();
			setIndexBuffer();
			mDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			mDevice->DrawIndexed(index_count, start_location, base_vertex_location);
		}

	private:

	};
	//------------------------------------------------------------------------------------
};