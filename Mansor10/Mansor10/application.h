#pragma once

#include "d3dapp.h"
#include <boost/shared_ptr.hpp>

class DemoApplication : public D3DApp
{
public:
    DemoApplication(HINSTANCE hInstance);
    ~DemoApplication();

    void initApp();
    void onResize();
    void updateScene(float dt);
    void drawScene();

private:
	D3DXMATRIX			mView;
	D3DXMATRIX			mProj;
	D3DXMATRIX			mWorld;
	D3DXMATRIX			mWVP;
	D3DXVECTOR3			mEyePos;

	boost::shared_ptr<Mesh>		mMesh;
	Material					mMtrl;
	Light 						mLights [4];

	float				mTheta;
	float				mPhi;
	float				mDistance;
	float				mHeight;
};