#include "gametimer.h"
#include <Windows.h>

GameTimer::GameTimer() : mSecondsPerCount(0.0), mDeltaTime(-1.0), mBaseTime(0),
	mPausedTime(0), mPrevTime(0), mCurrTime(0), mStopped(false)
{
	__int64 counts_per_sec;
	QueryPerformanceFrequency((LARGE_INTEGER*)&counts_per_sec);
	mSecondsPerCount = 1.0 / (double)counts_per_sec;
}

void GameTimer::tick()
{
	if(mStopped)
	{
		mDeltaTime = 0.0;
		return;
	}

	__int64 curr_time;
	QueryPerformanceCounter((LARGE_INTEGER*)&curr_time);
	mCurrTime = curr_time;

	mDeltaTime = (mCurrTime - mPrevTime) * mSecondsPerCount;

	mPrevTime = mCurrTime;
	if(mDeltaTime < 0.0)
	{
		mDeltaTime = 0.0;
	}
}

float GameTimer::getDeltaTime() const
{
	return (float)mDeltaTime;
}

void GameTimer::reset()
{
	__int64 curr_time;
	QueryPerformanceCounter((LARGE_INTEGER*)&curr_time);

	mBaseTime = curr_time;
	mPrevTime = curr_time;
	mStopTime = 0;
	mStopped = false;
}

void GameTimer::stop()
{
    if(!mStopped)
    {
        __int64 curr_time;
        QueryPerformanceCounter((LARGE_INTEGER*)&curr_time);

        mStopTime = curr_time;
        mStopped  = true;
    }
}

void GameTimer::start()
{
    __int64 start_time;
    QueryPerformanceCounter((LARGE_INTEGER*)&start_time);

    // Accumulate the time elapsed between stop and start pairs.
    //
    //                |<-------d------->|
    // ---------------*-----------------*------------> time
    //             mStopTime        startTime

	if(mStopped)
    {
        mPausedTime += (start_time - mStopTime);
        mPrevTime = start_time;
        mStopTime = 0;
        mStopped  = false;
    }
}

float GameTimer::getGameTime()const
{
    // If we are stopped, do not count the time that has passed since
    // we stopped.
    //
    // ----*---------------*------------------------------*------> time
    //  mBaseTime       mStopTime                      mCurrTime

    if( mStopped )
    {
        return (float)((mStopTime - mBaseTime) * mSecondsPerCount);
    }
    // The distance mCurrTime - mBaseTime includes paused time,
    // which we do not want to count. To correct this, we can subtract
    // the paused time from mCurrTime:
    //
    //  (mCurrTime - mPausedTime) - mBaseTime
    //
    //                     |<-------d------->|
    // ----*---------------*-----------------*------------*------> time
    // mBaseTime        mStopTime        startTime     mCurrTime
    else
    {
        return (float)(((mCurrTime - mPausedTime) - mBaseTime) * mSecondsPerCount);
    }
}



