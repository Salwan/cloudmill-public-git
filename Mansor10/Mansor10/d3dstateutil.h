#pragma once

#include <D3D10.h>
#include <D3DX10.h>

#include "d3dutil.h"

namespace StateUtil
{
	class RasterState
	{
	public:
		RasterState(ID3D10Device* device) : mDevice(device), bDirty(false)
		{
			assert(mDevice);

			ZeroMemory(&mRasterDesc, sizeof(D3D10_RASTERIZER_DESC));
			mRasterDesc.FillMode = D3D10_FILL_SOLID;
			mRasterDesc.CullMode = ::D3D10_CULL_BACK;
			mRasterDesc.FrontCounterClockwise = false;
			createRasterState();
		}

		virtual ~RasterState()
		{
			RELEASECOM(mRasterState);
		}

		void changeFillMode(D3D10_FILL_MODE fillmode)
		{
			if(fillmode != mRasterDesc.FillMode) bDirty = true;
			mRasterDesc.FillMode = fillmode;
		}

		void changeCullMode(D3D10_CULL_MODE cullmode)
		{
			if(cullmode != mRasterDesc.CullMode) bDirty = true;
			mRasterDesc.CullMode = cullmode;
		}

		void changeFrontCounterClockwise(BOOL frontccw)
		{
			if(frontccw != mRasterDesc.FrontCounterClockwise) bDirty = true;
			mRasterDesc.FrontCounterClockwise = frontccw;
		}

		void changeDepthBias(INT bias)
		{
			if(bias != mRasterDesc.DepthBias) bDirty = true;
			mRasterDesc.DepthBias = bias;
		}

		void changeDepthBiasClamp(FLOAT clamp)
		{
			if(clamp != mRasterDesc.DepthBiasClamp) 
			{
				bDirty = true;
				mRasterDesc.DepthBiasClamp = clamp;
			}
		}

		void changeSlopeScaledDepthBias(FLOAT bias)
		{
			if(bias != mRasterDesc.SlopeScaledDepthBias) 
			{
				bDirty = true;
				mRasterDesc.SlopeScaledDepthBias = bias;
			}
		}

		void changeDepthClipEnable(BOOL enable)
		{
			if(enable != mRasterDesc.DepthClipEnable) 
			{
				bDirty = true;
				mRasterDesc.DepthClipEnable = enable;
			}
		}

		void changeScissorEnable(BOOL enable)
		{
			if(enable != mRasterDesc.ScissorEnable) 
			{
				bDirty = true;
				mRasterDesc.ScissorEnable = enable;
			}
		}

		void changeMultisampleEnable(BOOL enable)
		{
			if(enable != mRasterDesc.MultisampleEnable) 
			{
				bDirty = true;
				mRasterDesc.MultisampleEnable = enable;
			}
		}

		void changeAntialiasedLineEnable(BOOL enable)
		{
			if(enable != mRasterDesc.AntialiasedLineEnable) 
			{
				bDirty = true;
				mRasterDesc.AntialiasedLineEnable = enable;
			}
		}

		void setRasterState()
		{
			if(bDirty)
			{
				RELEASECOM(mRasterState);
				createRasterState();
			}
			mDevice->RSSetState(mRasterState);
		}

	private:
		void createRasterState()
		{
			HR(mDevice->CreateRasterizerState(&mRasterDesc, &mRasterState));
		}

	private:
		bool						bDirty;
		ID3D10Device*				mDevice;
		D3D10_RASTERIZER_DESC		mRasterDesc;
		ID3D10RasterizerState*		mRasterState;
	};

	class BlendState
	{
	public:
		BlendState()
		{
		}

		virtual ~BlendState()
		{
		}

	private:

	};

	class DepthStencilState
	{
	public:
		DepthStencilState()
		{
		}

		virtual ~DepthStencilState()
		{
		}
	};
};

