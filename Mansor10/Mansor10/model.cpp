#include "model.h"
#include <sstream>

using namespace pugi;

Model::Model(ID3D10Device* device, const std::wstring& model_file, const std::wstring& effect_file, float scale)
	: mDevice(device), mScale(scale)
{
	assert(mDevice);

	parseColladaModel(model_file);

	//parseColladaFile(model_file);
	//createEffect(effect_file);
}


Model::~Model()
{
}

void Model::draw(const char* tech_name)
{
	/*
	float blendFactors[] = {0.0f, 0.0f, 0.0f, 0.0f};
	mDevice->OMSetBlendState(0, blendFactors, 0xffffffff);
	
	mEffect->applyInputLayout(tech_name, 0, *mElementDesc.get());
	mEffect->applyEffect(tech_name);
	mVB->setDrawVertexBuffer(mVB->getCount(), 0);
	*/
}

void Model::setMatrix(const char* name, const D3DXMATRIX& matrix)
{
	mEffect->setMatrix(name, (float*)&matrix);
}

void Model::parseColladaModel(const std::wstring& model_file)
{
	unsigned int count;
	xml_document colladaDoc;
	xml_parse_result result = colladaDoc.load_file(model_file.c_str());

	if(result.status != pugi::status_ok)
	{
		MessageBoxA(NULL, "Error parsing collada model file", result.description(), 0);
		throw std::exception("Model::parseColladaModel(): FAILED to parse collada model.");
	}

	Log::message(L">> Model::parseColladaModel() successfully loaded file `%s`\n", model_file.c_str());

	// <COLLADA> (always one)
	xml_node header = colladaDoc.child("COLLADA");

	// <library_visual_scenes> (always one)
	xml_node libVisualScenes = header.child("library_visual_scenes");

	// <visual_scene> (first one)
	xml_node visualScene = libVisualScenes.child("visual_scene");
	Log::message(L">> Parsing scene `%S`\n", visualScene.attribute("name").value());

	// <node>
	count = 0;
	for(xml_node n = visualScene.child("node"); n; n = n.next_sibling())
	{
		count++;
		Log::message(L">>>> Parsing node %d: `%S`\n", count, n.attribute("name").value());
		parseSceneNode(n);
	}

	// <library_geometries> (always one)
	xml_node libGeometries = header.child("library_geometries");
	Log::message(L">> Parsing geometries\n");

	// <geometry>
	count = 0;
	for(unsigned int i = 0; i < mNodes.size(); ++i)
	{
		count++;
		Log::message(L">>>> parsing geometry %d: `%S`\n", count, mNodes[i].geometry.id.c_str());
		xml_node nGeometry = libGeometries.find_child_by_attribute("id", mNodes[i].geometry.id.c_str());
		parseGeometry(nGeometry, mNodes[i].geometry);
	}
	//for(xml_node n = libGeometries.child("geometry"); n; n = n.next_sibling())
	//{
	//	count++;
	//	Log::message(L">>>> Parsing geometry %d: `%S`\n", count, n.attribute("id").value());
	//	parseGeometry(n);
	//}
}

void Model::parseSceneNode(const xml_node& node)
{
	// <node>
	SNode sceneNode;
	sceneNode.id = node.attribute("id").value();

	// <translate>
	for(xml_node n = node.child("translate"); n; n = n.next_sibling())
	{
			D3DXVECTOR3 vec;
			D3DXMATRIX t;
			parseInnerVec3(n, vec);
			D3DXMatrixTranslation(&t, vec.x, vec.y, vec.z);
			sceneNode.translation = sceneNode.translation * t;
	}

	// <rotate>
	for(xml_node n = node.child("rotate"); n; n = n.next_sibling())
	{
			D3DXQUATERNION q;
			D3DXMATRIX r;
			parseInnerQuat(n, q);
			D3DXMatrixRotationQuaternion(&r, &q);
			sceneNode.rotation = sceneNode.rotation * r;
	}

	// <scale sid="scale">
	for(xml_node n = node.child("scale"); n; n = n.next_sibling())
	{
			D3DXVECTOR3 vec;
			D3DXMATRIX s;
			parseInnerVec3(n, vec);
			D3DXMatrixScaling(&s, vec.x, vec.y, vec.z);
			sceneNode.scaling = sceneNode.scaling * s;
	}

	// <instance_geometry> (first one)
	xml_node instanceGeometry = node.child("instance_geometry");

	if(instanceGeometry)
	{
		sceneNode.geometry.id = instanceGeometry.attribute("url").value();
		if(sceneNode.geometry.id.at(0) == '#')
		{
			// cull first char
			sceneNode.geometry.id.assign(sceneNode.geometry.id.begin() + 1, sceneNode.geometry.id.end());
		}
		Log::message(L".. geometry id: `%S`\n", sceneNode.geometry.id.c_str());

		// <bind_material><technique_common> (opt)
		xml_node bindMaterial = instanceGeometry.child("bind_material");
		if(bindMaterial)
		{
			xml_node techniqueCommon = bindMaterial.child("technique_common");
			if(techniqueCommon)
			{
				Log::message(L".. found material binding secion\n");
				// <instance_material>
				for(xml_node n = techniqueCommon.child("instance_material"); n; n = n.next_sibling())
				{
					std::string materialTarget = n.attribute("target").value();
					if(materialTarget.at(0) == '#')
					{
						materialTarget.assign(materialTarget.begin() + 1, materialTarget.end());
					}
					Log::message(L".... found material: `%S`\n", materialTarget.c_str());
					// Add material to material group for later reference
					sceneNode.geometry.materials[materialTarget] = SMaterial();
				}
			}
			else
			{
				Log::message(L".. didn't find technique common section\n");
			}
		}
		else
		{
			Log::message(L".. didn't find material binding section\n");
		}
	}
	else
	{
		Log::message(L".. didn't find instance geometry section\n");
		throw std::exception();
	}

	// Add scene node to node array
	mNodes.push_back(sceneNode);
}

void Model::parseGeometry(const pugi::xml_node& node, SGeometry& geometry)
{
	// <mesh> (always one)
	xml_node nMesh = node.child("mesh");
	if(nMesh)
	{
		// <vertices> (always one) -- MUST always have 1 input with semantic = POSITION
		xml_node nVertices = nMesh.child("vertices");
		std::string verticesId; // id of <source> which includes vertex info
		if(nVertices)
		{
			xml_node semanticChild = nVertices.find_child_by_attribute("semantic", "POSITION");
			if(semanticChild)
			{
				verticesId = semanticChild.attribute("source").value();
				verticesId.assign(verticesId.begin() + 1, verticesId.end());
			}
			else
			{
				Log::message(L".. didn't find <vertices> with <input semantic=\"POSITION\" ...>");
				throw std::exception();
			}
		}
		else
		{
			Log::message(L".. didn't find vertices section\n");
			throw std::exception();
		}

		// <polygons>: 3DSMAX exporter, always 3 vertices form a triangle
		// <polylist>: Blender exporter, more standard, polygons can have more than
		//             3 vertices, and <vcount> has the count. Must tesselate manually.
		int polyCount = 0; // number of triangles
		for(xml_node n = nMesh.child("polygons"); n; n = n.next_sibling())
		{
			Log::message(L".. parsing <polygons> of material `%S`\n",
				n.attribute("material").value());
			polyCount += parsePolygons(n);
		}
		for(xml_node n = nMesh.child("polylist"); n; n = n.next_sibling())
		{
			Log::message(L".. parsing <polylist> of material `%S`\n",
				n.attribute("material").value());
			polyCount += parsePolylist(n);
		}
		if(polyCount == 0)
		{
			Log::message(L".. could'nt find not even one polygon!\n");
			throw std::exception();
		}
	}
	else
	{
		Log::message(L".. didn't find mesh section\n");
		throw std::exception();
	}
}

int Model::parsePolygons(const pugi::xml_node& node)
{
	// TODO
	return 0;
}

int Model::parsePolylist(const pugi::xml_node& node)
{
	// TODO
	return 0;
}

void Model::parseInnerVec3(const pugi::xml_node& node, D3DXVECTOR3& out)
{
	std::istringstream iss(node.child_value());
	iss >> out.x;
	iss >> out.y;
	iss >> out.z;
}

void Model::parseInnerQuat(const pugi::xml_node& node, D3DXQUATERNION& out)
{
	std::istringstream iss(node.child_value());
	D3DXVECTOR3 vec;
	float angle;
	iss >> vec.x;
	iss >> vec.y;
	iss >> vec.z;
	iss >> angle;
	angle = angle * TORADCONST;
	D3DXQuaternionRotationAxis(&out, &vec, angle);
	D3DXQuaternionNormalize(&out, &out);
}



void Model::parseColladaFile(const std::wstring& model_file)
{
	xml_document colladaDoc;
	xml_parse_result result = colladaDoc.load_file(model_file.c_str());

	if(result.status != pugi::status_ok)
	{
		MessageBoxA(NULL, "Error parsing collada model file", result.description(), 0);
		throw std::exception("Model::parseColladaFile(): FAILED to parse collada file.");
	}

	Log::message(L">> Model::parseColladaFile() successfully parsed file `%s`\n", model_file.c_str());

	xml_node header = colladaDoc.child("COLLADA");
	xml_node libGeometries = header.child("library_geometries");
	std::vector<xml_node> geometries;
	for(xml_node n = libGeometries.child("geometry"); n; n = n.next_sibling("geometry"))
	{
		geometries.push_back(n);
	}
	Log::message(L"> Found %d geometry objects\n", geometries.size());
	for(unsigned int i = 0; i < geometries.size(); ++i)
	{
		Log::message(L"> Geometry %d: %S\n", i, geometries[i].attribute("name").value());
	}

	// TEMP: for now just parse first Model (laters I should parse all and make them share buffers)
	parseGeometryOld(geometries[0]);
}


// TODO: this is a stupid parser, it should be rewritten to be much smarter and robust.
void Model::parseGeometryOld(const xml_node& geometry_node)
{
	// Read geometry info <geometry id="Box01-lib" name="Box01Mesh">
	const std::string strId = geometry_node.attribute("id").value();
	std::string strPosition = strId; strPosition.append("-Position");
	std::string strNormal = strId; strNormal.append("-Normal0");
	std::string strTexCoord = strId; strTexCoord.append("-UV0");

	// Get into mesh <mesh>
	xml_node mesh = geometry_node.child("mesh");
	xml_node source;

	// Read vertices <source id="Box01-lib-Position"><float_array id="Box01-lib-Position-array" count="24">
	source = mesh.find_child_by_attribute("source", "id", strPosition.c_str());
	xml_node nodeVertices = source.child("float_array");
	unsigned int floatCount = nodeVertices.attribute("count").as_uint();
	std::istringstream iss(nodeVertices.child_value());
	std::vector<D3DXVECTOR3> vertices;
	for(unsigned int i = 0; i < floatCount; i += 3)
	{
		D3DXVECTOR3 v;
		iss >> v.x;
		iss >> v.z;
		iss >> v.y;
		v.z = -v.z;
		//Log::message(L"- Vertex %d: i(%d) %f %f %f\n", vertices.size(), i, v.x, v.y, v.z);
		vertices.push_back(v);
	}
	unsigned int vertexCount = floatCount / 3;

	// Read normals <source id="Box01-lib-Normal0"><float_array id="Box01-lib-Normal0-array" count="108">
	source = mesh.find_child_by_attribute("source", "id", strNormal.c_str());
	xml_node nodeNormals = source.child("float_array");
	floatCount = nodeNormals.attribute("count").as_uint();
	iss = std::istringstream(nodeNormals.child_value());
	std::vector<D3DXVECTOR3> normals;
	for(unsigned int i = 0; i < floatCount; i += 3)
	{
		D3DXVECTOR3 v;
		iss >> v.x;
		iss >> v.z;
		iss >> v.y;
		v.z = -v.z;
		normals.push_back(v);
	}
	unsigned int normalCount = floatCount / 3;

	// Read texture coordinates <source id="Box01-lib-UV0"><float_array id="Box01-lib-UV0-array" count="24">
	source = mesh.find_child_by_attribute("source", "id", strTexCoord.c_str());
	xml_node nodeTexCoord = source.child("float_array");
	floatCount = nodeTexCoord.attribute("count").as_uint();
	iss = std::istringstream(nodeTexCoord.child_value());
	std::vector<D3DXVECTOR2> texcoord;
	for(unsigned int i = 0; i < floatCount; i += 2)
	{
		D3DXVECTOR2 v;
		iss >> v.x;
		iss >> v.y;
		texcoord.push_back(v);
	}
	unsigned int texCount = floatCount / 2;

	// Read indices <polygons count="12"><input semantic...><p>0 0 9 2 1 11 3 2 10</p>
	xml_node polygonsNode = mesh.child("polygons");
	unsigned int indexCount = polygonsNode.attribute("count").as_uint() * 9;
	// Collada as it is doesn't try to make ligitamte indices, so I either use only a vertex buffer, or I do the reduction myself.
	// TEMP: for now, I'll just use a vertex buffer.
	std::vector<DWORD> indices;
	for(xml_node n = polygonsNode.child("p"); n; n = n.next_sibling("p"))
	{
		DWORD index;
		iss = std::istringstream(n.child_value());
		for(unsigned int i = 0; i < 9; ++i)
		{
			iss >> index;
			indices.push_back(index);
		}
	}

	// Prepare element desc and vertices
	// TEMP: for now, the simplest, position-color vertex that uses Simple shader. color is random.
	mElementDesc.reset(new IAUtil::VertexElementDesc(PNTVertexDesc, PNTVertexElementCount));

	// Create vertex buffer
	vertexCount = indexCount / 3; // Real vertexCount

	mVB.reset(new ResUtil::VertexBuffer<PNTVertex>(mDevice, vertexCount));
	for(unsigned int i = 0; i < indexCount; i += 3)
	{
		PNTVertex v;
		v.pos = vertices[indices[i]] * mScale;
		v.normal = normals[indices[i + 1]];
		v.tex = texcoord[indices[i + 2]];
		Log::message(L"- %d Vertices: i(%d) (%f %f %f) (%f %f %f) (%f %f)\n",
			mVB->getCount(), i, v.pos.x, v.pos.y, v.pos.z, v.normal.x, v.normal.y, v.normal.z, v.tex.x, v.tex.y);
		mVB->addVertex(v);
	}
	mVB->createBuffer();

	Log::message(L"Vertex Count: %d\n", vertexCount, mVB->getCount());
	Log::message(L">> Done parsing collada model.\n");
}

void Model::createEffect(const std::wstring& effect_file)
{
	mEffect.reset(new FxUtil::Effect(mDevice, effect_file.c_str()));
}


