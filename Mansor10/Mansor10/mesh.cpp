#include "mesh.h"
#include <sstream>

using namespace pugi;

Mesh::Mesh(ID3D10Device* device, const std::wstring& model_file, const std::wstring& effect_file, float scale)
	: mDevice(device), mScale(scale)
{
	assert(mDevice);
	parseColladaFile(model_file);
	createEffect(effect_file);
}


Mesh::~Mesh()
{
}

void Mesh::draw(const char* tech_name)
{
	float blendFactors[] = {0.0f, 0.0f, 0.0f, 0.0f};
	mDevice->OMSetBlendState(0, blendFactors, 0xffffffff);
	
	mEffect->applyInputLayout(tech_name, 0, *mElementDesc.get());
	mEffect->applyEffect(tech_name);
	mVB->setDrawVertexBuffer(mVB->getCount(), 0);
}

void Mesh::setMatrix(const char* name, const D3DXMATRIX& matrix)
{
	mEffect->setMatrix(name, (float*)&matrix);
}

void Mesh::parseColladaFile(const std::wstring& model_file)
{
	xml_document colladaDoc;
	xml_parse_result result = colladaDoc.load_file(model_file.c_str());

	if(result.status != pugi::status_ok)
	{
		MessageBoxA(NULL, "Error parsing collada mesh file", result.description(), 0);
		throw std::exception("Mesh::parseColladaFile(): FAILED to parse collada file.");
	}

	Log::message(L">> Mesh::parseColladaFile() successfully parsed file `%s`\n", model_file.c_str());

	xml_node header = colladaDoc.child("COLLADA");
	xml_node libGeometries = header.child("library_geometries");
	std::vector<xml_node> geometries;
	for(xml_node n = libGeometries.child("geometry"); n; n = n.next_sibling("geometry"))
	{
		geometries.push_back(n);
	}
	Log::message(L"> Found %d geometry objects\n", geometries.size());
	for(unsigned int i = 0; i < geometries.size(); ++i)
	{
		Log::message(L"> Geometry %d: %S\n", i, geometries[i].attribute("name").value());
	}

	// TEMP: for now just parse first mesh (laters I should parse all and make them share buffers)
	parseGeometry(geometries[0]);
}


// TODO: this is a stupid parser, it should be rewritten to be much smarter and robust.
void Mesh::parseGeometry(const xml_node& geometry_node)
{
	// Read geometry info <geometry id="Box01-lib" name="Box01Mesh">
	const std::string strId = geometry_node.attribute("id").value();
	std::string strPosition = strId; strPosition.append("-Position");
	std::string strNormal = strId; strNormal.append("-Normal0");
	std::string strTexCoord = strId; strTexCoord.append("-UV0");

	// Get into mesh <mesh>
	xml_node mesh = geometry_node.child("mesh");
	xml_node source;

	// Read vertices <source id="Box01-lib-Position"><float_array id="Box01-lib-Position-array" count="24">
	source = mesh.find_child_by_attribute("source", "id", strPosition.c_str());
	xml_node nodeVertices = source.child("float_array");
	unsigned int floatCount = nodeVertices.attribute("count").as_uint();
	std::istringstream iss(nodeVertices.child_value());
	std::vector<D3DXVECTOR3> vertices;
	for(unsigned int i = 0; i < floatCount; i += 3)
	{
		D3DXVECTOR3 v;
		iss >> v.x;
		iss >> v.z;
		iss >> v.y;
		v.z = -v.z;
		//Log::message(L"- Vertex %d: i(%d) %f %f %f\n", vertices.size(), i, v.x, v.y, v.z);
		vertices.push_back(v);
	}
	unsigned int vertexCount = floatCount / 3;

	// Read normals <source id="Box01-lib-Normal0"><float_array id="Box01-lib-Normal0-array" count="108">
	source = mesh.find_child_by_attribute("source", "id", strNormal.c_str());
	xml_node nodeNormals = source.child("float_array");
	floatCount = nodeNormals.attribute("count").as_uint();
	iss = std::istringstream(nodeNormals.child_value());
	std::vector<D3DXVECTOR3> normals;
	for(unsigned int i = 0; i < floatCount; i += 3)
	{
		D3DXVECTOR3 v;
		iss >> v.x;
		iss >> v.z;
		iss >> v.y;
		v.z = -v.z;
		normals.push_back(v);
	}
	unsigned int normalCount = floatCount / 3;

	// Read texture coordinates <source id="Box01-lib-UV0"><float_array id="Box01-lib-UV0-array" count="24">
	source = mesh.find_child_by_attribute("source", "id", strTexCoord.c_str());
	xml_node nodeTexCoord = source.child("float_array");
	floatCount = nodeTexCoord.attribute("count").as_uint();
	iss = std::istringstream(nodeTexCoord.child_value());
	std::vector<D3DXVECTOR2> texcoord;
	for(unsigned int i = 0; i < floatCount; i += 2)
	{
		D3DXVECTOR2 v;
		iss >> v.x;
		iss >> v.y;
		texcoord.push_back(v);
	}
	unsigned int texCount = floatCount / 2;

	// Read indices <polygons count="12"><input semantic...><p>0 0 9 2 1 11 3 2 10</p>
	xml_node polygonsNode = mesh.child("polygons");
	unsigned int indexCount = polygonsNode.attribute("count").as_uint() * 9;
	// Collada as it is doesn't try to make ligitamte indices, so I either use only a vertex buffer, or I do the reduction myself.
	// TEMP: for now, I'll just use a vertex buffer.
	std::vector<DWORD> indices;
	for(xml_node n = polygonsNode.child("p"); n; n = n.next_sibling("p"))
	{
		DWORD index;
		iss = std::istringstream(n.child_value());
		for(unsigned int i = 0; i < 9; ++i)
		{
			iss >> index;
			indices.push_back(index);
		}
	}

	// Prepare element desc and vertices
	// TEMP: for now, the simplest, position-color vertex that uses Simple shader. color is random.
	mElementDesc.reset(new IAUtil::VertexElementDesc(PNTVertexDesc, PNTVertexElementCount));

	// Create vertex buffer
	vertexCount = indexCount / 3; // Real vertexCount

	mVB.reset(new ResUtil::VertexBuffer<PNTVertex>(mDevice, vertexCount));
	for(unsigned int i = 0; i < indexCount; i += 3)
	{
		PNTVertex v;
		v.pos = vertices[indices[i]] * mScale;
		v.normal = normals[indices[i + 1]];
		v.tex = texcoord[indices[i + 2]];
		Log::message(L"- %d Vertices: i(%d) (%f %f %f) (%f %f %f) (%f %f)\n",
			mVB->getCount(), i, v.pos.x, v.pos.y, v.pos.z, v.normal.x, v.normal.y, v.normal.z, v.tex.x, v.tex.y);
		mVB->addVertex(v);
	}
	mVB->createBuffer();

	Log::message(L"Vertex Count: %d\n", vertexCount, mVB->getCount());
	Log::message(L">> Done parsing collada mesh.\n");
}

void Mesh::createEffect(const std::wstring& effect_file)
{
	mEffect.reset(new FxUtil::Effect(mDevice, effect_file.c_str()));
}


