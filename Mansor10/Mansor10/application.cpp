#include "application.h"
const float PI = 22.0f / 7.0f;

int WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, PSTR cmd_line, int show_cmd)
{
	// Enable runtime memory check for debug
#if defined(DEBUG) || defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	DemoApplication app(instance);
	app.initApp();
	return app.run();
}

DemoApplication::DemoApplication(HINSTANCE hInstance) : D3DApp(hInstance)
{
	D3DXMatrixIdentity(&mView);
	D3DXMatrixIdentity(&mProj);
	D3DXMatrixIdentity(&mWorld);

	mPhi = 1.0f;
	mTheta = 1.0f;
	mDistance = 10.0f;
	mHeight = 0.0f;
}

DemoApplication::~DemoApplication()
{
	if( mDevice )
        mDevice->ClearState();
}

void DemoApplication::initApp()
{
    D3DApp::initApp();

	// Projection matrix
	float aspect = (float)mClientWidth / mClientHeight;
	D3DXMatrixPerspectiveFovLH(&mProj, 0.25f * PI, aspect, 1.0f, 1000.0f);

	// Load collada file
	mMesh.reset(new Mesh(mDevice, L"models/hipoly.dae", L"shaders/vertexlight.fx", 0.1f));
	Model* model = new Model(mDevice, L"models/multi.dae", L"shaders/vertexlight.fx", 0.1f);

	// Setup material and light
	mMtrl.diffuse = D3DXVECTOR4(0.6f, 0.4f, 0.5f, 1.0f);
	mMtrl.specular = D3DXVECTOR4(0.5f, 0.5f, 0.5f, 1.0f);
	mMtrl.power = 8.0f;


	memset(mLights, 0, sizeof(Light) * 4);

	// First light is point
	mLights[0].ambient = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);
	mLights[0].diffuse = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[0].specular = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[0].pos = D3DXVECTOR4(0.0f, 5.0f, -10.0f, 0.0f);
	mLights[0].att = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	mLights[0].range = 50.0f;

	// And a directional light
	mLights[1].ambient = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);
	mLights[1].diffuse = D3DXVECTOR4(0.8f, 0.5f, 0.3f, 1.0f);
	mLights[1].specular = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[1].dir = D3DXVECTOR4(1.0f, -0.5f, -1.0f, 0.0f);
	D3DXVec4Normalize(&mLights[1].dir, &mLights[1].dir);

	// And another point light
	mLights[2].ambient = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);
	mLights[2].diffuse = D3DXVECTOR4(0.0f, 0.0f, 0.8f, 1.0f);
	mLights[2].specular = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mLights[2].pos = D3DXVECTOR4(15.0f, 1.0f, -7.0f, 0.0f);
	mLights[2].att = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	mLights[2].range = 50.0f;


	int lightsDesc [] = {LIGHT_POINT, LIGHT_DIRECTIONAL, LIGHT_POINT, LIGHT_NONE};

	mMesh->getEffect()->setFloatVector("gAmbient", (float*)D3DXVECTOR4(0.1f, 0.1f, 0.1f, 1.0f));
	mMesh->getEffect()->setRaw("gLight", mLights, sizeof(Light) * 4);
	mMesh->getEffect()->setRaw("gLightType", lightsDesc, sizeof(int) * 4);
	mMesh->getEffect()->setObject<Material>("gMaterial", mMtrl);
	
	//mLight.dir = D3DXVECTOR3(1.0f, -0.5f, -1.0f);
	//D3DXVec3Normalize(&mLight.dir, &mLight.dir);
	//mLight.pos = D3DXVECTOR3(0.0f, 5.0f, -10.0f);
	//mLight.att = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	//mLight.range = 40.0f;
	////mLight.type = DIRECTIONAL_LIGHT;
	//mLight.type = 1.0f;
	//mLight.ambient = D3DXVECTOR4(0.1f, 0.1f, 0.1f, 1.0f);
	//mLight.diffuse = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	//mLight.specular = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	
	/*mDirLight.ambient = D3DXVECTOR4(0.1f, 0.1f, 0.1f, 1.0f);
	mDirLight.diffuse = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mDirLight.specular = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mDirLight.dir = D3DXVECTOR4(1.0f, -0.5f, -1.0f, 0.0f);

	mPointLight.ambient = D3DXVECTOR4(0.1f, 0.1f, 0.1f, 1.0f);
	mPointLight.diffuse = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mPointLight.specular = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mPointLight.att = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	mPointLight.pos = D3DXVECTOR4(0.0f, 5.0f, -10.0f, 0.0f);
	mPointLight.range = 50.0f;

	mSpotLight.ambient = D3DXVECTOR4(0.1f, 0.1f, 0.1f, 1.0f);
	mSpotLight.diffuse = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mSpotLight.specular = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f);
	mSpotLight.att = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	mSpotLight.pos = D3DXVECTOR4(0.0f, 5.0f, -10.0f, 0.0f);
	mSpotLight.range = 50.0f;
	mSpotLight.dir = D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.0f);
	mSpotLight.spotPower = 5.0f;

	mMesh->getEffect()->setMaterial("gMaterial", mMtrl);
	mMesh->getEffect()->setObject<PointLight>("gLight", mPointLight);
	mMesh->getEffect()->setObject<SpotLight>("gSpotLight", mSpotLight);*/
	//mMesh->getEffect()->setObject<DirLight>("gLight", mPointLight);
	//mMesh->getEffect()->setFloatVector("gAtt", (float*)&mPointLight.att);
}

void DemoApplication::onResize()
{
    D3DApp::onResize();

	float aspect = (float)mClientWidth / mClientHeight;
	D3DXMatrixPerspectiveFovLH(&mProj, 0.25f * PI, aspect, 1.0f, 1000.0f);
}

void DemoApplication::updateScene(float dt)
{
    D3DApp::updateScene(dt);

	if(GetAsyncKeyState('A') & 0x8000) mTheta	-= 2.0f * dt;
	if(GetAsyncKeyState('D') & 0x8000) mTheta	+= 2.0f * dt;
	if(GetAsyncKeyState('W') & 0x8000) mPhi		-= 2.0f * dt;
	if(GetAsyncKeyState('S') & 0x8000) mPhi		+= 2.0f * dt;
	if(GetAsyncKeyState(VK_DOWN) & 0x8000) mDistance += 25.0f * dt;
	if(GetAsyncKeyState(VK_UP) & 0x8000) mDistance -= 25.0f * dt;
	if(GetAsyncKeyState(VK_LEFT) & 0x8000) mHeight -= 25.0f * dt;
	if(GetAsyncKeyState(VK_RIGHT) & 0x8000) mHeight += 25.0f * dt;

	float x = mDistance * sinf(mPhi) * sinf(mTheta);
	float y = mDistance * cosf(mPhi);
	float z = -mDistance * sinf(mPhi) * cosf(mTheta);

	mEyePos = D3DXVECTOR3(x, y, z);
	D3DXVECTOR3 target(0.0f, mHeight, 0.0f);
	D3DXVECTOR3 up(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&mView, &mEyePos, &target, &up);

	static float curZ = -10.0f;
	static bool dirZ = true;
	if(dirZ)
	{
		curZ += 5.0f * dt;		
		if(curZ >= 10.0f)
		{
			dirZ = false;
			curZ = 10.0f;
		}
	}
	else
	{
		curZ -= 5.0f * dt;
		if(curZ <= -10.0f)
		{
			dirZ = true;
			curZ = -10.0f;
		}
	}
	mLights[0].pos.z = curZ;

	/*static float spotY = 0.0f;
	static float spotX = 0.0f;
	if(GetAsyncKeyState('I') & 0x8000) spotY += 2.0f * dt;
	if(GetAsyncKeyState('K') & 0x8000) spotY -= 2.0f * dt;
	if(GetAsyncKeyState('J') & 0x8000) spotX -= 2.0f * dt;
	if(GetAsyncKeyState('L') & 0x8000) spotX += 2.0f * dt;
	mSpotLight.dir.y = spotY;
	mSpotLight.dir.x = spotX;
	mSpotLight.dir.z = 1.0f;
	D3DXVec4Normalize(&mSpotLight.dir, &mSpotLight.dir);*/

	// Only update first light for now
	mMesh->getEffect()->setRaw("gLight", mLights, sizeof(Light));
}

void DemoApplication::drawScene()
{
    D3DApp::drawScene();

	// Draw
	mDevice->OMSetDepthStencilState(0, 0);
	float blendFactors[] = {0.0f, 0.0f, 0.0f, 0.0f};
	mDevice->OMSetBlendState(0, blendFactors, 0xffffffff);

	mWVP = mWorld * mView * mProj;

	mMesh->setMatrix("gWVP", mWVP);
	//mMesh->setMatrix("gWorld", mWorld);
	mMesh->getEffect()->setFloatVector("gEyePos", (float*)&mEyePos);

	mMesh->draw("LightTech");

    // We specify DT_NOCLIP, so we do not care about width/height
    // of the rect.
    RECT R = {5, 5, 0, 0};
    mFont->DrawText(0, mFrameStats.c_str(), -1, &R, DT_NOCLIP, WHITE);

    mSwapChain->Present(1, 0);
}
