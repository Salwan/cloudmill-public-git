#pragma once

#include "d3diautil.h"

#include <string>
#include <map>
#include <exception>
#include <boost/shared_ptr.hpp>

#include "light.h"
#include "material.h"

#define MAX_PASSES_PER_EFFECT 16

namespace FxUtil
{
	namespace Profile
	{
		static const char* VS_1_1 = "vs_1_1";
		static const char* PS_2_0 = "ps_2_0";
		static const char* PS_2_x = "ps_2_x";
		static const char* VS_2_0 = "vs_2_0";
		static const char* VS_2_x = "vs_2_x";
		static const char* PS_3_0 = "ps_3_0";
		static const char* VS_3_0 = "vs_3_0";
		static const char* GS_4_0 = "gs_4_0";
		static const char* PS_4_0 = "ps_4_0";
		static const char* VS_4_0 = "vs_4_0";
		static const char* GS_4_1 = "gs_4_1";
		static const char* PS_4_1 = "ps_4_1";
		static const char* VS_4_1 = "vs_4_1";
		static const char* FX_1_0 = "fx_1_0";
		static const char* FX_2_0 = "fx_2_0";
		static const char* FX_4_0 = "fx_4_0";
		static const char* FX_4_1 = "fx_4_1";
	};

	////////////////////////////////////////////////////////////////////
	// TODO
	// - Make custom element desc and count buffer IAUtil::VertexElementDesc for future usage
	//
	class Effect
	{
	public:
		Effect(ID3D10Device* device, const std::wstring& filename)
			: mDevice(device), mEffect(NULL), mEffectFilename(filename)
		{
			assert(mDevice);
			assert(filename.empty() == false);

			DWORD shader_flags = D3D10_SHADER_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
			shader_flags |= D3D10_SHADER_DEBUG;
			shader_flags |= D3D10_SHADER_SKIP_OPTIMIZATION;
#endif

			ID3D10Blob* err = NULL;
			HRESULT hr;
			hr = D3DX10CreateEffectFromFile(filename.c_str(), NULL, NULL, Profile::FX_4_0, 
				shader_flags, 0, mDevice, 0, 0, &mEffect, &err, 0);

			if(FAILED(hr))
			{
				if(err)
				{
					MessageBoxA(0, (char*)err->GetBufferPointer(), 0, 0);
					RELEASECOM(err);
				}
				DXTrace(__FILE__, (DWORD)__LINE__, hr, L"D3DX10CreateEffectFromFile", true);
			}
		}

		virtual ~Effect()
		{
			RELEASECOM(mEffect);
			if(mInputLayoutMap.size() > 0)
			{
				for(TInputLayoutMapIter i = mInputLayoutMap.begin();
					i != mInputLayoutMap.end();
					++i)
				{
					for(unsigned int j = 0; j < i->second.size(); ++j)
					{
						RELEASECOM(i->second[j].layout);
					}
				}
			}
		}

	private:
		enum EShaderVarType
		{
			E_VAR_RAW = 0,
			E_VAR_BOOL,
			E_VAR_FLOAT,
			E_VAR_INT,
			E_VAR_FLOAT_VECTOR,
			E_VAR_INT_VECTOR,
			E_VAR_MATRIX,
		};

		struct ShaderVarDesc
		{
			EShaderVarType							type;
			ID3D10EffectVariable*					variable;
		};

		struct InputLayoutDesc
		{
			InputLayoutDesc() : layout(NULL) {}
			ID3D10InputLayout*						layout;
		};

		// Used to buffer access to variables.
		std::map<std::string, ShaderVarDesc> mConstantMap;
		typedef std::map<std::string, ShaderVarDesc>::iterator TConstantMapIter;
		// Used to buffer input layout created by pass
		std::map<std::string, std::vector<InputLayoutDesc> > mInputLayoutMap;
		typedef std::map<std::string, std::vector<InputLayoutDesc> >::iterator TInputLayoutMapIter;

	public:
		// Activating
		void applyEffect(const char* tech_name, unsigned int pass = 0)
		{
			getTechnique(tech_name)->GetPassByIndex(pass)->Apply(0);
		}

		// Techniques
		ID3D10EffectTechnique* getTechnique(const char* name)
		{
			return mEffect->GetTechniqueByName(name);
		}

		// Passes
		unsigned int getPassCount(const char* tech_name)
		{
			D3D10_TECHNIQUE_DESC d;
			getTechnique(tech_name)->GetDesc(&d);
			return d.Passes;
		}

		// Input layout
		ID3D10InputLayout* getInputLayout(const char* tech_name, unsigned int pass, const IAUtil::VertexElementDesc& element_desc)
		{
			TInputLayoutMapIter iter = mInputLayoutMap.find(tech_name);
			if(iter == mInputLayoutMap.end())
			{
				// add technique to map
				mInputLayoutMap.insert(std::pair<std::string, std::vector<InputLayoutDesc> >(tech_name, std::vector<InputLayoutDesc>(MAX_PASSES_PER_EFFECT)));
			}
			// check if pass layout already buffered
			if(mInputLayoutMap[tech_name].size() >= pass && mInputLayoutMap[tech_name][pass].layout != NULL)
			{
				return mInputLayoutMap[tech_name][pass].layout;
			}
			// if not create it and add it
			ID3D10InputLayout* layout = NULL;
			D3D10_PASS_DESC pass_desc;
			getTechnique(tech_name)->GetPassByIndex(pass)->GetDesc(&pass_desc);
			HR(mDevice->CreateInputLayout(element_desc.getDesc(), element_desc.getDescCount(), pass_desc.pIAInputSignature,
				pass_desc.IAInputSignatureSize, &layout));
			mInputLayoutMap[tech_name][pass].layout = layout;
			return layout;
		}

		ID3D10InputLayout* getInputLayout(const char* tech_name, unsigned int pass, 
			const D3D10_INPUT_ELEMENT_DESC* element_desc, unsigned int element_count)
		{
			IAUtil::VertexElementDesc elementDesc(element_desc, element_count);
			return getInputLayout(tech_name, pass, elementDesc);
		}

		void applyInputLayout(const char* tech_name, unsigned int pass, IAUtil::VertexElementDesc& element_desc)
		{
			mDevice->IASetInputLayout(getInputLayout(tech_name, pass, element_desc));
		}

		void applyInputLayout(const char* tech_name, unsigned int pass, 
			const D3D10_INPUT_ELEMENT_DESC* element_desc, unsigned int element_count)
		{
			IAUtil::VertexElementDesc elementDesc(element_desc, element_count);
			mDevice->IASetInputLayout(getInputLayout(tech_name, pass, elementDesc));
		}

		// Object methods
		template <typename T>
		void setObject(const char* name, const T& object)
		{
			setRaw(name, (void*)&object, sizeof(T), 0);
		}

		template <typename T>
		void getObject(const char* name, const T& out_object)
		{
			getRaw(name, (void*)&out_object, sizeof(T), 0);
		}

		void setMaterial(const char* name, const Material& material)
		{
			setRaw(name, (void*)&material, sizeof(Material), 0);
		}

		void getMaterial(const char* name, const Material& out_material)
		{
			getRaw(name, (void*)&out_material, sizeof(Material), 0);
		}

		// Raw methods
		void setRaw(const char* name, void* data, unsigned int count, unsigned int offset = 0)
		{
			getScalarVariable(name, E_VAR_RAW)->SetRawValue(data, offset, count);
		}

		void getRaw(const char* name, void* out_data, unsigned int count, unsigned int offset = 0)
		{
			getScalarVariable(name, E_VAR_RAW)->GetRawValue(out_data, offset, count);
		}

		// Scalar methods
		void setBool(const char* name, BOOL value)
		{
			getScalarVariable(name, E_VAR_BOOL)->AsScalar()->SetBool(value);
		}

		void setFloat(const char* name, float value)
		{
			getScalarVariable(name, E_VAR_FLOAT)->AsScalar()->SetFloat(value);
		}

		void setInt(const char* name, int value)
		{
			getScalarVariable(name, E_VAR_INT)->AsScalar()->SetInt(value);
		}

		BOOL getBool(const char* name)
		{
			BOOL value;
			getScalarVariable(name, E_VAR_BOOL)->AsScalar()->GetBool(&value);
			return value;
		}

		float getFloat(const char* name)
		{
			float value;
			getScalarVariable(name, E_VAR_FLOAT)->AsScalar()->GetFloat(&value);
			return value;
		}

		int getInt(const char* name)
		{
			int value;
			getScalarVariable(name, E_VAR_INT)->AsScalar()->GetInt(&value);
			return value;
		}

		// Vector methods
		void setFloatVector(const char* name, float* vector4)
		{
			getScalarVariable(name, E_VAR_FLOAT_VECTOR)->AsVector()->SetFloatVector(vector4);
		}

		void setIntVector(const char* name, int* vector4)
		{
			getScalarVariable(name, E_VAR_INT_VECTOR)->AsVector()->SetIntVector(vector4);
		}

		void getFloatVector(const char* name, float* out_vector4)
		{
			getScalarVariable(name, E_VAR_FLOAT_VECTOR)->AsVector()->GetFloatVector(out_vector4);
		}

		void getIntVector(const char* name, int* out_vector4)
		{
			getScalarVariable(name, E_VAR_INT_VECTOR)->AsVector()->GetIntVector(out_vector4);
		}

		// Matrix methods
		void setMatrix(const char* name, float* matrix)
		{
			getScalarVariable(name, E_VAR_MATRIX)->AsMatrix()->SetMatrix(matrix);
		}

		void setMatrixTranspose(const char* name, float* matrix)
		{
			getScalarVariable(name, E_VAR_MATRIX)->AsMatrix()->SetMatrixTranspose(matrix);
		}

		void getMatrix(const char* name, float* out_matrix)
		{
			getScalarVariable(name, E_VAR_MATRIX)->AsMatrix()->GetMatrix(out_matrix);
		}

		void getMatrixTranspose(const char* name, float* out_matrix)
		{
			getScalarVariable(name, E_VAR_MATRIX)->AsMatrix()->GetMatrixTranspose(out_matrix);
		}

	private:
		bool isVarBuffered(const char* name, EShaderVarType type, TConstantMapIter* iterator)
		{
			TConstantMapIter iter = mConstantMap.find(name);
			if(iter != mConstantMap.end() && iter->second.type == type)
			{
				*iterator = iter;
				return true;
			}
			else
			{
				return false;
			}
		}

		void addVarToBuffer(const char* name, EShaderVarType type, ID3D10EffectVariable* effect_var)
		{
			ShaderVarDesc d;
			d.type = type;
			d.variable = effect_var;
			mConstantMap.insert(std::pair<std::string, ShaderVarDesc>(name, d));
		}

		ID3D10EffectVariable* getScalarVariable(const char* name, EShaderVarType type)
		{
			TConstantMapIter iterator;
			if(isVarBuffered(name, type, &iterator))
			{
				// Already buffered, use the buffer.
				return iterator->second.variable;
			}
			else
			{
				ID3D10EffectVariable* var = mEffect->GetVariableByName(name);
				if(mEffect->IsValid())
				{
					// Variable exists! buffer
					addVarToBuffer(name, E_VAR_BOOL, var);
					return var;
				}
				else
				{
					// Variable doesn't exist, bitch about it...
					wchar_t buffer [1024];
					swprintf_s(buffer, 1024, L"getScalarVariable could not find variable %s in effect file %s", name, mEffectFilename.c_str());
					MessageBoxW(NULL, buffer, L"Effect Error", 0);
					throw std::exception();
				}
			}
		}

	private:
		ID3D10Device*			mDevice;
		ID3D10Effect*			mEffect;
		std::wstring			mEffectFilename;
	};
}

