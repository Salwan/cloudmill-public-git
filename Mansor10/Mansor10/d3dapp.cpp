#include "d3dapp.h"
#include <sstream>

static D3DApp* ApplicationInstance = NULL;
LRESULT CALLBACK MessageProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

D3DApp::D3DApp(HINSTANCE instance) : mInstanceHandle(instance), mDevice(NULL), mClientWidth(1024), mClientHeight(768), mFont(NULL)
{
	ApplicationInstance = this;
	Log::message(L"========================================\n");
	Log::message(L"<<<<< Welcome to Mansour 10 Engine >>>>>\n");
	Log::message(L"========================================\n");

}

D3DApp::~D3DApp()
{
	RELEASECOM(mDevice);
	RELEASECOM(mSwapChain);
	RELEASECOM(mFont);
	RELEASECOM(mDepthStencilView);
	RELEASECOM(mRenderTargetView);
	RELEASECOM(mDepthStencilBuffer);
}

int D3DApp::run()
{
	mTimer.start();

	MSG msg = {0};

    while(msg.message != WM_QUIT)
    {
        if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }        
        else // Otherwise, do animation/game stuff.
        {
			mTimer.tick();
			updateScene(mTimer.getDeltaTime());
			drawScene();
        }
    }
    return (int)msg.wParam;
}

void D3DApp::initApp()
{
	try
	{
		initMainWindow();
		initDirect3D();
	}
	catch(std::exception* e)
	{
		MessageBoxA(NULL, e->what(), "Exception Thrown", 0);
	}
}

void D3DApp::onResize()
{
}

void D3DApp::updateScene(float dt)
{
    static int frameCnt = 0;
	static float t_accum = 0.0f;

    frameCnt++;
	t_accum += dt;

    // Compute averages over one second period.
    if( t_accum >= 1.0f )
    {
        float fps = (float)frameCnt; // fps = frameCnt / 1
        float mspf = 1000.0f / fps;

        std::wostringstream outs;
        outs.precision(6);
        outs << L"FPS: " << fps << L"\n"
             << "ms/frame: " << mspf;

        // Save the stats in a string for output.
        mFrameStats = outs.str();

        // Reset for next average.
        frameCnt = 0;

		t_accum = 0.0f;
    }
}

void D3DApp::drawScene()
{
    mDevice->ClearRenderTargetView(mRenderTargetView, mClearColor);
    mDevice->ClearDepthStencilView(mDepthStencilView, D3D10_CLEAR_DEPTH|D3D10_CLEAR_STENCIL, 1.0f, 0);
}

LRESULT D3DApp::messageProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_ACTIVATE:
		if(LOWORD(wParam) == WA_INACTIVE)
		{
			mPaused = true;
			mTimer.stop();
		}
		else
		{
			mPaused = false;
			mTimer.start();
		}
		return 0;

	case WM_ENTERSIZEMOVE:
		mPaused = true;
		mResizing = true;
		mTimer.stop();
		return 0;

	case WM_EXITSIZEMOVE:
		mPaused = false;
		mResizing = false;
		mTimer.start();
		onResize();
		return 0;

	case WM_MENUCHAR:	// Don't beep when alt-enter
		return MAKELRESULT(0, MNC_CLOSE);

	case WM_GETMINMAXINFO:
		((MINMAXINFO*)lParam)->ptMinTrackSize.x = 200;
		((MINMAXINFO*)lParam)->ptMinTrackSize.y = 200;
		return 0;

    case WM_KEYDOWN:
        if( wParam == VK_ESCAPE )
            DestroyWindow(mWindowHandle);
        return 0;

    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    }

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void D3DApp::initMainWindow()
{
	Log::message(L">> Initializing Main Window...");
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = MessageProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = mInstanceHandle;
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = L"D3DApp";

	if(!RegisterClass(&wc))
	{
		throw new std::exception("Register Class FAILED");
	}

	mWindowHandle = CreateWindow(
		L"D3DApp",
		L"Mansour 10 Framework (Direct3D 10)",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		mClientWidth,
		mClientHeight,
		0,
		0,
		mInstanceHandle,
		0);

	if(mWindowHandle == 0)
	{
		wchar_t buf [128];
		swprintf(buf, 128, L"> LAST ERROR: %d\n", GetLastError());
		OutputDebugString(buf);
		throw new std::exception("Create Window FAILED");
	}

	ShowWindow(mWindowHandle, SW_SHOW);
	UpdateWindow(mWindowHandle);

	Log::message(L"success\n");
}

void D3DApp::initDirect3D()
{
	HRESULT hr; 
	Log::message(L">> Initializing Direct3D 10...");

	// Fill swap chain description
	DXGI_SWAP_CHAIN_DESC scdesc;
	scdesc.BufferDesc.Width = mClientWidth;
	scdesc.BufferDesc.Height = mClientHeight;
	scdesc.BufferDesc.RefreshRate.Numerator = 0;
	scdesc.BufferDesc.RefreshRate.Denominator = 60;
	scdesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scdesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	scdesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	scdesc.SampleDesc.Count = 1;
	scdesc.SampleDesc.Quality = 0;
	scdesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scdesc.BufferCount = 1;
	scdesc.OutputWindow = mWindowHandle;
	scdesc.Windowed = true;
	scdesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	scdesc.Flags = 0;

	// Create device and swap chain
	UINT create_device_flags = 0;
//#if defined(_DEBUG) || defined(DEBUG)
//	create_device_flags |= D3D10_CREATE_DEVICE_DEBUG;
//#endif
	hr = D3D10CreateDeviceAndSwapChain(0, D3D10_DRIVER_TYPE_HARDWARE, 
		0, create_device_flags, D3D10_SDK_VERSION, &scdesc, &mSwapChain, &mDevice);
	if(FAILED(hr))
	{
		throw new std::exception("D3D10 Create Device and Swap Chain FAILED");
	}

	// Create render target view for swap chain back buffer
	ID3D10Texture2D* backbuffer;
	mSwapChain->GetBuffer(0, __uuidof(ID3D10Texture2D), reinterpret_cast<void**>(&backbuffer));
	mDevice->CreateRenderTargetView(backbuffer, 0, &mRenderTargetView);
	RELEASECOM(backbuffer);

	// Create depth stencil target and view
	D3D10_TEXTURE2D_DESC dsdesc;
	dsdesc.Width              = mClientWidth;
	dsdesc.Height             = mClientHeight;
	dsdesc.MipLevels          = 1;
	dsdesc.ArraySize          = 1;
	dsdesc.Format             = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsdesc.SampleDesc.Count   = 1; // multisampling must match
	dsdesc.SampleDesc.Quality = 0; // swap chain values.
	dsdesc.Usage              = D3D10_USAGE_DEFAULT;
	dsdesc.BindFlags          = D3D10_BIND_DEPTH_STENCIL;
	dsdesc.CPUAccessFlags     = 0;
	dsdesc.MiscFlags          = 0;

	hr = mDevice->CreateTexture2D(&dsdesc, 0, &mDepthStencilBuffer);
	if(FAILED(hr))
	{
		throw new std::exception("Creating depth stencil buffer FAILED");
	}
	hr = mDevice->CreateDepthStencilView(mDepthStencilBuffer, 0, &mDepthStencilView);
	if(FAILED(hr))
	{
		throw new std::exception("Creating depth stencil view FAILED");
	}

	// Bind render target view and depth/stencil view to the output merger rendering stage
	mDevice->OMSetRenderTargets(1, &mRenderTargetView, mDepthStencilView);

	// Set the viewport
	D3D10_VIEWPORT viewport;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = mClientWidth;
	viewport.Height = mClientHeight;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	
	mDevice->RSSetViewports(1, &viewport);

	Log::message(L"success\n");

	// Initialize Debug Font
	D3DX10_FONT_DESC fontdesc;
	fontdesc.Height          = 18;
	fontdesc.Width           = 0;
	fontdesc.Weight          = 0;
	fontdesc.MipLevels       = 1;
	fontdesc.Italic          = false;
	fontdesc.CharSet         = DEFAULT_CHARSET;
	fontdesc.OutputPrecision = OUT_DEFAULT_PRECIS;
	fontdesc.Quality         = DEFAULT_QUALITY;
	fontdesc.PitchAndFamily  = DEFAULT_PITCH | FF_DONTCARE;
	wcscpy_s(fontdesc.FaceName, 32, L"Arial");
	D3DX10CreateFontIndirect(mDevice, &fontdesc, &mFont);
}

LRESULT CALLBACK MessageProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if(ApplicationInstance)
	{
		return ApplicationInstance->messageProc(hWnd, msg, wParam, lParam);
	}
	else
	{
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}
