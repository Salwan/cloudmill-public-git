#include "d3diautil.h"

using namespace IAUtil;



// ============== VertexElementDesc ================
VertexElementDesc::VertexElementDesc(unsigned int element_count) 
	: mElementCount(element_count), mElementIndex(0)
{
	mElementDesc.reset(new D3D10_INPUT_ELEMENT_DESC [mElementCount]);
	for(unsigned int i = 0; i < mElementCount; ++i)
	{
		mElementDesc[i].SemanticName = NULL;
	}
}

VertexElementDesc::VertexElementDesc(const D3D10_INPUT_ELEMENT_DESC* element_desc, unsigned int element_count)
	: mElementCount(element_count), mElementIndex(0)
{
	assert(element_desc);
	mElementDesc.reset(new D3D10_INPUT_ELEMENT_DESC [mElementCount]);
	memcpy(mElementDesc.get(), element_desc, sizeof(D3D10_INPUT_ELEMENT_DESC) * mElementCount);
	mElementIndex = mElementCount;
}

VertexElementDesc::~VertexElementDesc()
{
}

D3D10_INPUT_ELEMENT_DESC* VertexElementDesc::getDesc() const
{
	return mElementDesc.get();
}

int VertexElementDesc::getDescCount() const
{
	return mElementIndex;
}

void VertexElementDesc::addElement(const char* semantic_const, EElementFormat format)
{
	if(mElementIndex >= mElementCount)
	{
		throw std::exception("VertexElementDesc: the element desc table defiend is already full!");
	}
	if(!semantic_const)
	{
		throw std::exception("VertexElementDesc: did you forget to pass a semantic const? that's rude...");
	}
	mElementDesc[mElementIndex].SemanticName = semantic_const;
	mElementDesc[mElementIndex].SemanticIndex = getSemanticIndex(semantic_const);
	mElementDesc[mElementIndex].Format = (DXGI_FORMAT)format;
	mElementDesc[mElementIndex].InputSlot = 0;
	mElementDesc[mElementIndex].AlignedByteOffset = D3D10_APPEND_ALIGNED_ELEMENT;
	mElementDesc[mElementIndex].InputSlotClass = D3D10_INPUT_PER_VERTEX_DATA;
	mElementDesc[mElementIndex].InstanceDataStepRate = 0;
	mElementIndex++;
}

int VertexElementDesc::getSemanticIndex(const char* semantic_const) const
{
	int index = 0;
	for(unsigned int i = 0; i < mElementCount; ++i)
	{
		if(semantic_const == mElementDesc[i].SemanticName)
		{
			index++;
		}
	}
	return index;
}