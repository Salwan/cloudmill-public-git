#pragma once
#include "d3dutil.h"

#define LIGHT_NONE 0
#define LIGHT_DIRECTIONAL 1
#define LIGHT_POINT 2
#define LIGHT_SPOT 3

struct Light
{
	Light()
	{
		memset(this, 0, sizeof(Light));
	}
	D3DXVECTOR4 pos;
	D3DXVECTOR4 dir;
	D3DXVECTOR4 ambient;
	D3DXVECTOR4 diffuse;
	D3DXVECTOR4 specular;
	D3DXVECTOR3 att;
	float		range;
	float		spotPower;
	D3DXVECTOR3 padding;
};