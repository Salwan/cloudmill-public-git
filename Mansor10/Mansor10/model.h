#pragma once
#include <boost/shared_ptr.hpp>
#include "d3dutil.h"
#include "d3diautil.h"
#include "d3dresutil.h"
#include "d3dfxutil.h"
#include "pugixml\pugixml.hpp"

// ============= Model ==============
class Model
{
private:
	struct SSemantic
	{
		SSemantic()
		{
			offset = 0;
			set = 0;
		}
		std::string semantic;
		unsigned int offset;
		unsigned int set;
	};
	struct SMaterial
	{
		SMaterial()
		{
			polyCount = -1;
			vertexBufferOffset = -1;
		}
		Material material;
		unsigned int polyCount;
		unsigned int vertexBufferOffset;
		boost::shared_ptr<IAUtil::VertexElementDesc> mElementDesc;
	};
	typedef std::map<std::string, SMaterial> MaterialGroup;
	struct SGeometry
	{
		SGeometry()
		{
			polyCount = -1;
			vertexBufferOffset = -1;
		}
		std::string id;
		unsigned int polyCount;
		unsigned int vertexBufferOffset;
		MaterialGroup materials;
		std::vector<SSemantic> mSemantics;
		boost::shared_ptr<IAUtil::VertexElementDesc> mElementDesc;
	};
	struct SNode
	{
		SNode()
		{
			D3DXMatrixIdentity(&translation);
			D3DXMatrixIdentity(&rotation);
			D3DXMatrixIdentity(&scaling);
		}
		std::string id;
		D3DXMATRIX translation;
		D3DXMATRIX rotation;
		D3DXMATRIX scaling;
		SGeometry geometry;
	};

public:
	Model(ID3D10Device* device, const std::wstring& model_file, const std::wstring& effect_file, float scale = 1.0f);
	virtual ~Model();

public:
	void draw(const char* tech_name);
	void setMatrix(const char* name, const D3DXMATRIX& matrix);
	FxUtil::Effect* getEffect() { return mEffect.get(); }

private:
	void parseColladaModel(const std::wstring& model_file);
	void parseSceneNode(const pugi::xml_node& node);
	void parseGeometry(const pugi::xml_node& node, SGeometry& geometry);
	// returns poly count
	int parsePolygons(const pugi::xml_node& node); 
	// returns poly count
	int parsePolylist(const pugi::xml_node& node);

	void parseInnerVec3(const pugi::xml_node& node, D3DXVECTOR3& out);
	void parseInnerQuat(const pugi::xml_node& node, D3DXQUATERNION& out);

	void createEffect(const std::wstring& effect_file);

	// DEPRECATED
	void parseColladaFile(const std::wstring& model_file);
	void parseGeometryOld(const pugi::xml_node& geometry_node);

private:
	ID3D10Device*										mDevice;
	boost::shared_ptr<IAUtil::VertexElementDesc>		mElementDesc;
	boost::shared_ptr<ResUtil::VertexBuffer<PNTVertex> > mVB;
	boost::shared_ptr<ResUtil::IndexBufferTriangleList> mIB;
	boost::shared_ptr<FxUtil::Effect>					mEffect;
	float												mScale;

	// Required data for model to be drawn correctly
	std::vector<SNode> mNodes;
};

