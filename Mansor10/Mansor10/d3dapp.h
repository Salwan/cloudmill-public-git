#pragma once
#include <windows.h>
#include <string>
#include <D3D10.h>
#include <D3DX10.h>
#include <DxErr.h>

#include "d3dutil.h"
#include "d3diautil.h"
#include "d3dresutil.h"
#include "d3dstateutil.h"
#include "d3dfxutil.h"
#include "mesh.h"
#include "model.h"
#include "gametimer.h"

#pragma comment(lib, "d3d10.lib")
#ifdef _DEBUG
#pragma comment(lib, "d3dx10d.lib")
#else
#pragma comment(lib, "d3dx10.lib")
#endif
#pragma comment(lib, "DxErr.lib")


class D3DApp
{
public:
	D3DApp(HINSTANCE instance);
	virtual ~D3DApp();

	HINSTANCE getAppInstanceHandle() const {return mInstanceHandle;}
	HWND getAppWindowHandle() const {return mWindowHandle;}

	int run();

	virtual void initApp();
	virtual void onResize();
	virtual void updateScene(float dt);
	virtual void drawScene();
	virtual LRESULT messageProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

protected:
	void initMainWindow();
	void initDirect3D();

protected:
	HINSTANCE mInstanceHandle;
	HWND mWindowHandle;
	bool mPaused;
	bool mMinimized;
	bool mMaximized;
	bool mResizing;

	GameTimer mTimer;

	std::wstring mFrameStats;

	ID3D10Device* mDevice;
	IDXGISwapChain* mSwapChain;
	ID3D10Texture2D* mDepthStencilBuffer;
	ID3D10RenderTargetView* mRenderTargetView;
	ID3D10DepthStencilView* mDepthStencilView;
	ID3DX10Font* mFont;

	std::wstring mWindowTitle;
	D3DXCOLOR mClearColor;

	int mClientWidth;
	int mClientHeight;
};